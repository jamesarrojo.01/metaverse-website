<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontController extends Controller
{
  public function index(){

    return view('FrontEnd.home');
  }

  public function privacy_policy(){

    return view('privacy-policy');
  }

  public function header(){

    return view('FrontEnd.header');
  }
}

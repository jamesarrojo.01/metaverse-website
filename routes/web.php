<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Game;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\Events;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
}); */

Route::get('/', [FrontController::class,'index'])->name('Front'); 

Route::get('/#about-us', [FrontController::class,'index'])->name('about-us'); 

Route::get('/header', [FrontController::class,'header'])->name('header'); 

Route::get('/privacy-policy', [FrontController::class,'privacy_policy'])->name('privacy'); 

Route::get('/game', [Game::class,'RunGame'])->name('RunGame');

Route::get('/events', [Events::class,'Events'])->name('Events');
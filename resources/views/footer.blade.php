
<footer itemtype="https://schema.org/WPFooter" itemscope="itemscope" id="colophon" role="contentinfo">
			<div class='footer-width-fixer'>		<div data-elementor-type="wp-post" data-elementor-id="1063" class="elementor elementor-1063">
									<section id="footer" class="elementor-section elementor-top-section elementor-element elementor-element-14065d8 elementor-section-content-middle elementor-hidden-tablet elementor-hidden-mobile elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="14065d8" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-2e8a3a5" data-id="2e8a3a5" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-48a6166 hfe-nav-menu__breakpoint-mobile hfe-nav-menu__align-left hfe-submenu-icon-arrow hfe-link-redirect-child elementor-widget elementor-widget-navigation-menu" data-id="48a6166" data-element_type="widget" data-settings="{&quot;padding_vertical_menu_item&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;padding_horizontal_menu_item&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:15,&quot;sizes&quot;:[]},&quot;padding_horizontal_menu_item_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;padding_horizontal_menu_item_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;padding_vertical_menu_item_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;padding_vertical_menu_item_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;menu_space_between&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;menu_space_between_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;menu_space_between_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;dropdown_border_radius&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;top&quot;:&quot;&quot;,&quot;right&quot;:&quot;&quot;,&quot;bottom&quot;:&quot;&quot;,&quot;left&quot;:&quot;&quot;,&quot;isLinked&quot;:true},&quot;dropdown_border_radius_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;top&quot;:&quot;&quot;,&quot;right&quot;:&quot;&quot;,&quot;bottom&quot;:&quot;&quot;,&quot;left&quot;:&quot;&quot;,&quot;isLinked&quot;:true},&quot;dropdown_border_radius_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;top&quot;:&quot;&quot;,&quot;right&quot;:&quot;&quot;,&quot;bottom&quot;:&quot;&quot;,&quot;left&quot;:&quot;&quot;,&quot;isLinked&quot;:true},&quot;padding_horizontal_dropdown_item&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;padding_horizontal_dropdown_item_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;padding_horizontal_dropdown_item_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;padding_vertical_dropdown_item&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:15,&quot;sizes&quot;:[]},&quot;padding_vertical_dropdown_item_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;padding_vertical_dropdown_item_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;distance_from_menu&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;distance_from_menu_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;distance_from_menu_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;toggle_size&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;toggle_size_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;toggle_size_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;toggle_border_width&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;toggle_border_width_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;toggle_border_width_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;toggle_border_radius&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;toggle_border_radius_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;toggle_border_radius_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]}}" data-widget_type="navigation-menu.default">
				<div class="elementor-widget-container">
						<div class="hfe-nav-menu hfe-layout-vertical hfe-nav-menu-layout vertical" data-layout="vertical">
				<div class="hfe-nav-menu__toggle elementor-clickable">
					<div class="hfe-nav-menu-icon">
						<i aria-hidden="true" tabindex="0" class="fas fa-align-justify"></i>					</div>
				</div>
				<nav class="hfe-nav-menu__layout-vertical hfe-nav-menu__submenu-arrow" data-toggle-icon="&lt;i aria-hidden=&quot;true&quot; tabindex=&quot;0&quot; class=&quot;fas fa-align-justify&quot;&gt;&lt;/i&gt;" data-close-icon="&lt;i aria-hidden=&quot;true&quot; tabindex=&quot;0&quot; class=&quot;far fa-window-close&quot;&gt;&lt;/i&gt;" data-full-width="yes"><ul id="menu-1-48a6166" class="hfe-nav-menu"><li id="menu-item-1012" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item parent hfe-creative-menu"><a href="https://ihk.8a1.myftpupload.com/metaverse-space/" class = "hfe-menu-item">Home</a></li>
<li id="menu-item-1013" class="menu-item menu-item-type-custom menu-item-object-custom parent hfe-creative-menu"><a href="#about" class = "hfe-menu-item">About</a></li>
<li id="menu-item-1014" class="menu-item menu-item-type-custom menu-item-object-custom parent hfe-creative-menu"><a href="#footer" class = "hfe-menu-item">Contact Us</a></li>
<li id="menu-item-1015" class="menu-item menu-item-type-custom menu-item-object-custom parent hfe-creative-menu"><a href="https:/        /ihk.8a1.myftpupload.com/blog-page/" class = "hfe-menu-item">Events</a></li>
</ul></nav>              
			</div>
					</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-e75c300" data-id="e75c300" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-fcc08af elementor-widget elementor-widget-image" data-id="fcc08af" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
																<a href="https://ihk.8a1.myftpupload.com/metaverse-space/">
							<img width="287" height="49" src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/logo.png?time=1663577346" class="attachment-medium size-medium" alt="" loading="lazy" />								</a>
															</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-60f659a" data-id="60f659a" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-aed3349 elementor-widget elementor-widget-heading" data-id="aed3349" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h3 class="elementor-heading-title elementor-size-default">Newsletter</h3>		</div>
				</div>
				<div class="elementor-element elementor-element-e950f0c elementor-widget elementor-widget-text-editor" data-id="e950f0c" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							Sign up for our newsletter to get the latest updates &amp; special offers						</div>
				</div>
				<div class="elementor-element elementor-element-600bb44 elementor-widget elementor-widget-premium-contact-form" data-id="600bb44" data-element_type="widget" data-widget_type="premium-contact-form.default">
				<div class="elementor-widget-container">
			
			<div class="premium-cf7-container">
				<div role="form" class="wpcf7" id="wpcf7-f1144-o1" lang="en-US" dir="ltr">
<div class="screen-reader-response"><p role="status" aria-live="polite" aria-atomic="true"></p> <ul></ul></div>
<form action="https://ihk.8a1.myftpupload.com/metaverse-space/#wpcf7-f1144-o1" method="post" class="wpcf7-form init" novalidate="novalidate" data-status="init">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="1144" />
<input type="hidden" name="_wpcf7_version" value="5.6.3" />
<input type="hidden" name="_wpcf7_locale" value="en_US" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f1144-o1" />
<input type="hidden" name="_wpcf7_container_post" value="0" />
<input type="hidden" name="_wpcf7_posted_data_hash" value="" />
</div>
<div class="field-btn-container"><span class="wpcf7-form-control-wrap" data-name="your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email" /></span> <input type="submit" value="Subscribe" class="wpcf7-form-control has-spinner wpcf7-submit" /></div>
<div class="wpcf7-response-output" aria-hidden="true"></div></form></div>			</div>

					</div>
				</div>
				<div class="elementor-element elementor-element-566788f elementor-shape-square e-grid-align-left elementor-grid-0 elementor-widget elementor-widget-social-icons" data-id="566788f" data-element_type="widget" data-widget_type="social-icons.default">
				<div class="elementor-widget-container">
					<div class="elementor-social-icons-wrapper elementor-grid">
							<span class="elementor-grid-item">
					<a class="elementor-icon elementor-social-icon elementor-social-icon-twitter elementor-repeater-item-a346a69" target="_blank">
						<span class="elementor-screen-only">Twitter</span>
						<i class="fab fa-twitter"></i>					</a>
				</span>
							<span class="elementor-grid-item">
					<a class="elementor-icon elementor-social-icon elementor-social-icon-discord elementor-repeater-item-1849c40" target="_blank">
						<span class="elementor-screen-only">Discord</span>
						<i class="fab fa-discord"></i>					</a>
				</span>
							<span class="elementor-grid-item">
					<a class="elementor-icon elementor-social-icon elementor-social-icon-medium elementor-repeater-item-5958a94" target="_blank">
						<span class="elementor-screen-only">Medium</span>
						<i class="fab fa-medium"></i>					</a>
				</span>
							<span class="elementor-grid-item">
					<a class="elementor-icon elementor-social-icon elementor-social-icon-telegram elementor-repeater-item-d084222" target="_blank">
						<span class="elementor-screen-only">Telegram</span>
						<i class="fab fa-telegram"></i>					</a>
				</span>
					</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-52bc7b8 elementor-section-content-middle elementor-hidden-desktop elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="52bc7b8" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-eca0efb" data-id="eca0efb" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-ef28d9f elementor-widget elementor-widget-image" data-id="ef28d9f" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="287" height="49" src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/logo.png?time=1663577346" class="attachment-medium size-medium" alt="" loading="lazy" />															</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-5bf936b" data-id="5bf936b" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-8f37d27 elementor-widget elementor-widget-heading" data-id="8f37d27" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h3 class="elementor-heading-title elementor-size-default">Newsletter</h3>		</div>
				</div>
				<div class="elementor-element elementor-element-e5b0b26 elementor-widget elementor-widget-text-editor" data-id="e5b0b26" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							Sign up for our newsletter to get the latest updates &amp; special offers						</div>
				</div>
				<div class="elementor-element elementor-element-d08a437 elementor-widget elementor-widget-premium-contact-form" data-id="d08a437" data-element_type="widget" data-widget_type="premium-contact-form.default">
				<div class="elementor-widget-container">
			
			<div class="premium-cf7-container">
				<div role="form" class="wpcf7" id="wpcf7-f1144-o2" lang="en-US" dir="ltr">
<div class="screen-reader-response"><p role="status" aria-live="polite" aria-atomic="true"></p> <ul></ul></div>
<form action="https://ihk.8a1.myftpupload.com/metaverse-space/#wpcf7-f1144-o2" method="post" class="wpcf7-form init" novalidate="novalidate" data-status="init">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="1144" />
<input type="hidden" name="_wpcf7_version" value="5.6.3" />
<input type="hidden" name="_wpcf7_locale" value="en_US" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f1144-o2" />
<input type="hidden" name="_wpcf7_container_post" value="0" />
<input type="hidden" name="_wpcf7_posted_data_hash" value="" />
</div>
<div class="field-btn-container"><span class="wpcf7-form-control-wrap" data-name="your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email" /></span> <input type="submit" value="Subscribe" class="wpcf7-form-control has-spinner wpcf7-submit" /></div>
<div class="wpcf7-response-output" aria-hidden="true"></div></form></div>			</div>

					</div>
				</div>
				<div class="elementor-element elementor-element-56daa6c elementor-shape-square e-grid-align-left e-grid-align-mobile-center elementor-grid-0 elementor-widget elementor-widget-social-icons" data-id="56daa6c" data-element_type="widget" data-widget_type="social-icons.default">
				<div class="elementor-widget-container">
					<div class="elementor-social-icons-wrapper elementor-grid">
							<span class="elementor-grid-item">
					<a class="elementor-icon elementor-social-icon elementor-social-icon-twitter elementor-repeater-item-a346a69" target="_blank">
						<span class="elementor-screen-only">Twitter</span>
						<i class="fab fa-twitter"></i>					</a>
				</span>
							<span class="elementor-grid-item">
					<a class="elementor-icon elementor-social-icon elementor-social-icon-discord elementor-repeater-item-1849c40" target="_blank">
						<span class="elementor-screen-only">Discord</span>
						<i class="fab fa-discord"></i>					</a>
				</span>
							<span class="elementor-grid-item">
					<a class="elementor-icon elementor-social-icon elementor-social-icon-medium elementor-repeater-item-5958a94" target="_blank">
						<span class="elementor-screen-only">Medium</span>
						<i class="fab fa-medium"></i>					</a>
				</span>
							<span class="elementor-grid-item">
					<a class="elementor-icon elementor-social-icon elementor-social-icon-telegram elementor-repeater-item-d084222" target="_blank">
						<span class="elementor-screen-only">Telegram</span>
						<i class="fab fa-telegram"></i>					</a>
				</span>
					</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-1456151 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="1456151" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-ba10db6" data-id="ba10db6" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-ba68258 elementor-widget elementor-widget-text-editor" data-id="ba68258" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p class="p1">Copyright © 2022 <span style="color:#fff">Metaverse Space</span> All rights reserved</p>						</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-c02da02" data-id="c02da02" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-bd6ef14 elementor-widget elementor-widget-text-editor" data-id="bd6ef14" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p><a href="{{route('privacy')}}">Privacy &amp; Cookies</a>          Terms Of Use      About</p>						</div>
				</div>
					</div>
		</div>
							</div>
		</section>
							</div>
		</div>		</footer>
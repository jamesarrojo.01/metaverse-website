<!DOCTYPE html>
<html lang="en-US">

<!-- Mirrored from ihk.8a1.myftpupload.com/metaverse-space/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 19 Sep 2022 09:02:08 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="https://gmpg.org/xfn/11">

<title>Metaverse Space</title>
<meta name='robots' content='noindex, nofollow' />
<link rel='dns-prefetch' href='http://fonts.googleapis.com/' />
<link rel='dns-prefetch' href='http://s.w.org/' />
<link rel="alternate" type="application/rss+xml" title="Fellaz &raquo; Feed" href="https://ihk.8a1.myftpupload.com/feed/" />
<link rel="alternate" type="application/rss+xml" title="Fellaz &raquo; Comments Feed" href="https://ihk.8a1.myftpupload.com/comments/feed/" />
<link rel='preconnect' href='https://secureservercdn.net/' crossorigin />
<link rel="stylesheet" href="{{ asset('css/home.css') }}">
<link rel="stylesheet" href="{{ asset('css/fixed-style.css') }}">
<script>
window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/14.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/14.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/secureservercdn.net\/198.71.190.114\/ihk.8a1.myftpupload.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=6.0.2&time=1663577346"}};
/*! This file is auto-generated */
!function(e,a,t){var n,r,o,i=a.createElement("canvas"),p=i.getContext&&i.getContext("2d");function s(e,t){var a=String.fromCharCode,e=(p.clearRect(0,0,i.width,i.height),p.fillText(a.apply(this,e),0,0),i.toDataURL());return p.clearRect(0,0,i.width,i.height),p.fillText(a.apply(this,t),0,0),e===i.toDataURL()}function c(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(o=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},r=0;r<o.length;r++)t.supports[o[r]]=function(e){if(!p||!p.fillText)return!1;switch(p.textBaseline="top",p.font="600 32px Arial",e){case"flag":return s([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])?!1:!s([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!s([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]);case"emoji":return!s([129777,127995,8205,129778,127999],[129777,127995,8203,129778,127999])}return!1}(o[r]),t.supports.everything=t.supports.everything&&t.supports[o[r]],"flag"!==o[r]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[o[r]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(e=t.source||{}).concatemoji?c(e.concatemoji):e.wpemoji&&e.twemoji&&(c(e.twemoji),c(e.wpemoji)))}(window,document,window._wpemojiSettings);
</script>



<!--[if IE]>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/themes/astra/assets/js/minified/flexibility.min.js?ver=3.9.1&#038;time=1663577346' id='astra-flexibility-js'></script>
<script id='astra-flexibility-js-after'>
flexibility(document.documentElement);
</script>
<![endif]-->
<script src="{{ asset('js/fixed-scripts.js') }}"></script>

<link rel="https://api.w.org/" href="https://ihk.8a1.myftpupload.com/wp-json/" /><link rel="alternate" type="application/json" href="https://ihk.8a1.myftpupload.com/wp-json/wp/v2/pages/1008" /><link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://ihk.8a1.myftpupload.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-includes/wlwmanifest.xml?time=1663577346" /> 
<link rel="canonical" href="https://ihk.8a1.myftpupload.com/metaverse-space/" />
<link rel='shortlink' href='https://ihk.8a1.myftpupload.com/?p=1008' />
<link rel="alternate" type="application/json+oembed" href="https://ihk.8a1.myftpupload.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fihk.8a1.myftpupload.com%2Fmetaverse-space%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://ihk.8a1.myftpupload.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fihk.8a1.myftpupload.com%2Fmetaverse-space%2F&amp;format=xml" />
				
		</head>

<body itemtype='https://schema.org/WebPage' itemscope='itemscope' class="page-template page-template-elementor_header_footer page page-id-1008 page-parent ehf-header ehf-footer ehf-template-astra ehf-stylesheet-astra group-blog ast-single-post ast-inherit-site-logo-transparent ast-hfb-header ast-desktop ast-page-builder-template ast-no-sidebar astra-3.9.1 elementor-default elementor-template-full-width elementor-kit-7 elementor-page elementor-page-1008">

<a
	class="skip-link screen-reader-text"
	href="#content"
	role="link"
	title="Skip to content">
		Skip to content</a>

<div class="hfeed site" id="page">
	@include('header')

		<div id="content" class="site-content">
		<div class="ast-container">
				<div data-elementor-type="wp-page" data-elementor-id="1008" class="elementor elementor-1008">
									<section class="elementor-section elementor-top-section elementor-element elementor-element-77b6e76d elementor-section-height-full elementor-section-items-bottom elementor-section-boxed elementor-section-height-default" data-id="77b6e76d" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;video&quot;,&quot;background_video_link&quot;:&quot;http:\/\/ihk.8a1.myftpupload.com\/wp-content\/uploads\/2022\/09\/VL_video_with-music2.mp4&quot;,&quot;background_video_start&quot;:0,&quot;background_play_on_mobile&quot;:&quot;yes&quot;}">
								<div class="elementor-background-video-container">
													<video class="elementor-background-video-hosted elementor-html5-video" autoplay muted playsinline loop></video>
											</div>
									<div class="elementor-background-overlay"></div>
							<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-126b104d" data-id="126b104d" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-18023d7 elementor-widget elementor-widget-heading" data-id="18023d7" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">The</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-14abfb9 elementor-widget elementor-widget-heading" data-id="14abfb9" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Meta<span style="color:#6EC1E4; font-family: Good Times !important;">verse</span></h2>		</div>
				</div>
				<div class="elementor-element elementor-element-b1e0b78 elementor-align-center elementor-widget elementor-widget-button" data-id="b1e0b78" data-element_type="widget" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="{{route('RunGame')}}" class="elementor-button-link elementor-button elementor-size-md" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">ENTER SPACE</span>
		</span>
					</a>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-21cdff1 elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id="21cdff1" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-68e2480" data-id="68e2480" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-0299b1a meta-partners elementor-widget elementor-widget-image-carousel" data-id="0299b1a" data-element_type="widget" data-settings="{&quot;slides_to_show&quot;:&quot;6&quot;,&quot;slides_to_scroll&quot;:&quot;3&quot;,&quot;navigation&quot;:&quot;none&quot;,&quot;lazyload&quot;:&quot;yes&quot;,&quot;autoplay_speed&quot;:1,&quot;speed&quot;:6000,&quot;image_spacing_custom&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:24,&quot;sizes&quot;:[]},&quot;slides_to_show_tablet&quot;:&quot;5&quot;,&quot;slides_to_show_mobile&quot;:&quot;2&quot;,&quot;slides_to_scroll_tablet&quot;:&quot;4&quot;,&quot;autoplay&quot;:&quot;yes&quot;,&quot;pause_on_hover&quot;:&quot;yes&quot;,&quot;pause_on_interaction&quot;:&quot;yes&quot;,&quot;infinite&quot;:&quot;yes&quot;}" data-widget_type="image-carousel.default">
				<div class="elementor-widget-container">
					<div class="elementor-image-carousel-wrapper swiper-container" dir="ltr">
			<div class="elementor-image-carousel swiper-wrapper">
								<div class="swiper-slide"><figure class="swiper-slide-inner"><img class="swiper-slide-image swiper-lazy" data-src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/Group-17.png?time=1663577346" alt="Group 17" /><div class="swiper-lazy-preloader"></div></figure></div><div class="swiper-slide"><figure class="swiper-slide-inner"><img class="swiper-slide-image swiper-lazy" data-src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/Mask-Group-15.png?time=1663577346" alt="Mask Group 15" /><div class="swiper-lazy-preloader"></div></figure></div><div class="swiper-slide"><figure class="swiper-slide-inner"><img class="swiper-slide-image swiper-lazy" data-src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/Mask-Group-16.png?time=1663577346" alt="Mask Group 16" /><div class="swiper-lazy-preloader"></div></figure></div><div class="swiper-slide"><figure class="swiper-slide-inner"><img class="swiper-slide-image swiper-lazy" data-src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/Mask-Group-18.png?time=1663577346" alt="Mask Group 18" /><div class="swiper-lazy-preloader"></div></figure></div><div class="swiper-slide"><figure class="swiper-slide-inner"><img class="swiper-slide-image swiper-lazy" data-src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/Mask-Group-19.png?time=1663577346" alt="Mask Group 19" /><div class="swiper-lazy-preloader"></div></figure></div><div class="swiper-slide"><figure class="swiper-slide-inner"><img class="swiper-slide-image swiper-lazy" data-src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/Mask-Group-20.png?time=1663577346" alt="Mask Group 20" /><div class="swiper-lazy-preloader"></div></figure></div>			</div>
																</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-4486d74 elementor-section-height-full elementor-section-boxed elementor-section-height-default elementor-section-items-middle" data-id="4486d74" id="about-us"data-element_type="section" id="about" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
							<div class="elementor-background-overlay"></div>
							<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-6f7a6ed" data-id="6f7a6ed" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-7ddd5e5 elementor-widget elementor-widget-heading" data-id="7ddd5e5" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">About Us</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-f75eef3 elementor-widget elementor-widget-text-editor" data-id="f75eef3" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit.<BR/>Vestibulum commodo diam vel placerat luctus. Suspendisse eget risus lectus.<br/>
Maecenas luctus nibh id diam iaculis						</div>
				</div>
				<div class="elementor-element elementor-element-ead3acf elementor-align-center elementor-widget elementor-widget-button" data-id="ead3acf" data-element_type="widget" data-widget_type="button.default">
				<div class="elementor-widget-container">
	
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-caa3b58 elementor-section-height-full elementor-section-boxed elementor-section-height-default elementor-section-items-middle" data-id="caa3b58" data-element_type="section" id="events" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
							<div class="elementor-background-overlay"></div>
							<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-cea346d" data-id="cea346d" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-242cc24 elementor-widget elementor-widget-heading" data-id="242cc24" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">What’s New?</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-0468bfd elementor-widget elementor-widget-text-editor" data-id="0468bfd" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum						</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-9a7ec87 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="9a7ec87" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-f072b0b" data-id="f072b0b" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-17e5a17 elementor-widget elementor-widget-image" data-id="17e5a17" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="546" height="395" src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/Untitled-3_03.jpg?time=1663577346" class="attachment-large size-large" alt="" loading="lazy" srcset="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/Untitled-3_03.jpg 546w, https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/Untitled-3_03-300x217.jpg 300w" sizes="(max-width: 546px) 100vw, 546px" />															</div>
				</div>
				<div class="elementor-element elementor-element-de7fed1 elementor-widget elementor-widget-text-editor" data-id="de7fed1" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							Upcoming Event						</div>
				</div>
				<div class="elementor-element elementor-element-a222f80 elementor-widget elementor-widget-heading" data-id="a222f80" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Spooky Night Halloween Special</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-d1d9617 elementor-widget elementor-widget-text-editor" data-id="d1d9617" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							When Halloween rolls around, black cat face painting&#8230;						</div>
				</div>
				<div class="elementor-element elementor-element-cbf19b6 elementor-icon-list--layout-traditional elementor-list-item-link-full_width elementor-widget elementor-widget-icon-list" data-id="cbf19b6" data-element_type="widget" data-widget_type="icon-list.default">
				<div class="elementor-widget-container">
			<link rel="stylesheet" href="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/elementor/assets/css/widget-icon-list.min.css">		<ul class="elementor-icon-list-items">
							<li class="elementor-icon-list-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="far fa-calendar"></i>						</span>
										<span class="elementor-icon-list-text">01/02/2022</span>
									</li>
						</ul>
				</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-152f331" data-id="152f331" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-603dca9 elementor-widget elementor-widget-image" data-id="603dca9" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="546" height="395" src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/Untitled-3_05.jpg?time=1663577346" class="attachment-large size-large" alt="" loading="lazy" srcset="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/Untitled-3_05.jpg 546w, https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/Untitled-3_05-300x217.jpg 300w" sizes="(max-width: 546px) 100vw, 546px" />															</div>
				</div>
				<div class="elementor-element elementor-element-1ebf78d elementor-widget elementor-widget-text-editor" data-id="1ebf78d" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							Patchnote						</div>
				</div>
				<div class="elementor-element elementor-element-649d4c3 elementor-widget elementor-widget-heading" data-id="649d4c3" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">New Item Chest</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-5a90204 elementor-widget elementor-widget-text-editor" data-id="5a90204" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							Winter is just around the corner and now that the new&#8230;						</div>
				</div>
				<div class="elementor-element elementor-element-6ba186a elementor-icon-list--layout-traditional elementor-list-item-link-full_width elementor-widget elementor-widget-icon-list" data-id="6ba186a" data-element_type="widget" data-widget_type="icon-list.default">
				<div class="elementor-widget-container">
					<ul class="elementor-icon-list-items">
							<li class="elementor-icon-list-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="far fa-calendar"></i>						</span>
										<span class="elementor-icon-list-text">01/02/2022</span>
									</li>
						</ul>
				</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-b6d97f8" data-id="b6d97f8" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-eedd78e elementor-widget elementor-widget-image" data-id="eedd78e" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="546" height="395" src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/Untitled-3_07.jpg?time=1663577346" class="attachment-large size-large" alt="" loading="lazy" srcset="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/Untitled-3_07.jpg 546w, https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/Untitled-3_07-300x217.jpg 300w" sizes="(max-width: 546px) 100vw, 546px" />															</div>
				</div>
				<div class="elementor-element elementor-element-4426163 elementor-widget elementor-widget-text-editor" data-id="4426163" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							Patchnote						</div>
				</div>
				<div class="elementor-element elementor-element-3702133 elementor-widget elementor-widget-heading" data-id="3702133" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">New Game Map</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-71550c0 elementor-widget elementor-widget-text-editor" data-id="71550c0" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							Christmas is Here! Metaverse Christmas party event&#8230;						</div>
				</div>
				<div class="elementor-element elementor-element-955587a elementor-icon-list--layout-traditional elementor-list-item-link-full_width elementor-widget elementor-widget-icon-list" data-id="955587a" data-element_type="widget" data-widget_type="icon-list.default">
				<div class="elementor-widget-container">
					<ul class="elementor-icon-list-items">
							<li class="elementor-icon-list-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="far fa-calendar"></i>						</span>
										<span class="elementor-icon-list-text">01/02/2022</span>
									</li>
						</ul>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-b33861c elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="b33861c" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_bottom&quot;:&quot;split&quot;}">
					<div class="elementor-shape elementor-shape-bottom" data-negative="false">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 20" preserveAspectRatio="none">
	<path class="elementor-shape-fill" d="M0,0v3c0,0,393.8,0,483.4,0c9.2,0,16.6,7.4,16.6,16.6c0-9.1,7.4-16.6,16.6-16.6C606.2,3,1000,3,1000,3V0H0z"/>
</svg>		</div>
					<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-dfd4d80" data-id="dfd4d80" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-1bda78b elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="1bda78b" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-7f9bb69" data-id="7f9bb69" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-aab1b07 elementor-widget elementor-widget-heading" data-id="aab1b07" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">PLAY 
fOR 
free </h2>		</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-b20cd80 " data-id="b20cd80" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-1b6b086 elementor-align-right elementor-mobile-align-center elementor-widget elementor-widget-button" data-id="1b6b086" data-element_type="widget" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="{{route('RunGame')}}" class="elementor-button-link elementor-button elementor-size-md" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">ENTER SPACE</span>
		</span>
					</a>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</section>
							</div>
			</div> <!-- ast-container -->
	</div><!-- #content -->

	<!-- footer -->
@include('footer')

		</div><!-- #page -->
        <div id="wpfront-scroll-top-container">
            <img src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/wpfront-scroll-top/images/icons/117.png" alt="" />        </div>
                <script type="text/javascript">
            function wpfront_scroll_top_init() {
                if (typeof wpfront_scroll_top === "function" && typeof jQuery !== "undefined") {
                    wpfront_scroll_top({"scroll_offset":100,"button_width":0,"button_height":0,"button_opacity":0.8000000000000000444089209850062616169452667236328125,"button_fade_duration":200,"scroll_duration":400,"location":1,"marginX":20,"marginY":20,"hide_iframe":false,"auto_hide":false,"auto_hide_after":2,"button_action":"top","button_action_element_selector":"","button_action_container_selector":"html, body","button_action_element_offset":0});
                } else {
                    setTimeout(wpfront_scroll_top_init, 100);
                }
            }
            wpfront_scroll_top_init();
        </script>
        <script id='astra-theme-js-js-extra'>
var astra = {"break_point":"921","isRtl":""};
</script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/themes/astra/assets/js/minified/frontend.min.js?ver=3.9.1&amp;time=1663577346' id='astra-theme-js-js'></script>
<script id='pa-frontend-js-extra'>
var PremiumSettings = {"ajaxurl":"https:\/\/ihk.8a1.myftpupload.com\/wp-admin\/admin-ajax.php","nonce":"7945eb99a1"};
</script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/premium-addons-elementor/pa-frontend-96a2e4a97.min.js?ver=1663578007&amp;time=1663577346' id='pa-frontend-js'></script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/coblocks/dist/js/coblocks-animation.js?ver=2.24.2&amp;time=1663577346' id='coblocks-animation-js'></script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/contact-form-7/includes/swv/js/index.js?ver=5.6.3&amp;time=1663577346' id='swv-js'></script>
<script id='contact-form-7-js-extra'>
var wpcf7 = {"api":{"root":"https:\/\/ihk.8a1.myftpupload.com\/wp-json\/","namespace":"contact-form-7\/v1"}};
</script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/contact-form-7/includes/js/index.js?ver=5.6.3&amp;time=1663577346' id='contact-form-7-js'></script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/wpfront-scroll-top/js/wpfront-scroll-top.min.js?ver=2.0.7.08086&amp;time=1663577346' id='wpfront-scroll-top-js'></script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/wpmss/wpmssab.min.js?ver=1662387883&amp;time=1663577346' id='wpmssab-js'></script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/mousewheel-smooth-scroll/js/SmoothScroll.min.js?ver=1.4.10&amp;time=1663577346' id='SmoothScroll-js'></script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/wpmss/wpmss.min.js?ver=1662387883&amp;time=1663577346' id='wpmss-js'></script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/header-footer-elementor/inc/js/frontend.js?ver=1.6.13&amp;time=1663577346' id='hfe-frontend-js-js'></script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/elementor/assets/js/webpack.runtime.min.js?ver=3.7.3&amp;time=1663577346' id='elementor-webpack-runtime-js'></script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/elementor/assets/js/frontend-modules.min.js?ver=3.7.3&amp;time=1663577346' id='elementor-frontend-modules-js'></script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2&amp;time=1663577346' id='elementor-waypoints-js'></script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-includes/js/jquery/ui/core.min.js?ver=1.13.1&amp;time=1663577346' id='jquery-ui-core-js'></script>
<script id='elementor-frontend-js-before'>
var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false,"isScriptDebug":false},"i18n":{"shareOnFacebook":"Share on Facebook","shareOnTwitter":"Share on Twitter","pinIt":"Pin it","download":"Download","downloadImage":"Download image","fullscreen":"Fullscreen","zoom":"Zoom","share":"Share","playVideo":"Play Video","previous":"Previous","next":"Next","close":"Close"},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"responsive":{"breakpoints":{"mobile":{"label":"Mobile","value":767,"default_value":767,"direction":"max","is_enabled":true},"mobile_extra":{"label":"Mobile Extra","value":880,"default_value":880,"direction":"max","is_enabled":false},"tablet":{"label":"Tablet","value":1024,"default_value":1024,"direction":"max","is_enabled":true},"tablet_extra":{"label":"Tablet Extra","value":1200,"default_value":1200,"direction":"max","is_enabled":false},"laptop":{"label":"Laptop","value":1366,"default_value":1366,"direction":"max","is_enabled":false},"widescreen":{"label":"Widescreen","value":2400,"default_value":2400,"direction":"min","is_enabled":false}}},"version":"3.7.3","is_static":false,"experimentalFeatures":{"e_dom_optimization":true,"e_optimized_assets_loading":true,"e_optimized_css_loading":true,"a11y_improvements":true,"additional_custom_breakpoints":true,"e_import_export":true,"e_hidden_wordpress_widgets":true,"landing-pages":true,"elements-color-picker":true,"favorite-widgets":true,"admin-top-bar":true},"urls":{"assets":"https:\/\/ihk.8a1.myftpupload.com\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"editorPreferences":[]},"kit":{"active_breakpoints":["viewport_mobile","viewport_tablet"],"global_image_lightbox":"yes","lightbox_enable_counter":"yes","lightbox_enable_fullscreen":"yes","lightbox_enable_zoom":"yes","lightbox_enable_share":"yes","lightbox_title_src":"title","lightbox_description_src":"description"},"post":{"id":1008,"title":"Metaverse%20Space%20%E2%80%93%20Fellaz","excerpt":"","featuredImage":false}};
</script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/elementor/assets/js/frontend.min.js?ver=3.7.3&amp;time=1663577346' id='elementor-frontend-js'></script>
			<script>
			/(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())},!1);
			</script>
					<script>'undefined'=== typeof _trfq || (window._trfq = []);'undefined'=== typeof _trfd && (window._trfd=[]),_trfd.push({'tccl.baseHost':'secureserver.net'}),_trfd.push({'ap':'wpaas'},{'server':'b8127770-513f-0487-f4e6-1f4ebf92fb61.secureserver.net'},{'pod':'P3NLWPPOD10'},{'storage':'p3cephmah004pod10_data10'},{'xid':'44806276'},{'wp':'6.0.2'},{'php':'8.0.22'},{'loggedin':'0'},{'cdn':'1'},{'builder':'elementor'},{'theme':'astra'},{'wds':'0'},{'wp_alloptions_count':'221'},{'wp_alloptions_bytes':'81997'})</script>
		<script>window.addEventListener('click', function (elem) { var _elem$target, _elem$target$dataset, _window, _window$_trfq; return (elem === null || elem === void 0 ? void 0 : (_elem$target = elem.target) === null || _elem$target === void 0 ? void 0 : (_elem$target$dataset = _elem$target.dataset) === null || _elem$target$dataset === void 0 ? void 0 : _elem$target$dataset.eid) && ((_window = window) === null || _window === void 0 ? void 0 : (_window$_trfq = _window._trfq) === null || _window$_trfq === void 0 ? void 0 : _window$_trfq.push(["cmdLogEvent", "click", elem.target.dataset.eid]));});</script>
		<script src='https://img1.wsimg.com/tcc/tcc_l.combined.1.0.6.min.js'></script>
		<script src='https://img1.wsimg.com/traffic-assets/js/tccl-tti.min.js' onload="window.tti.calculateTTI()"></script>
			</body>

<!-- Mirrored from ihk.8a1.myftpupload.com/metaverse-space/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 19 Sep 2022 09:02:08 GMT -->
</html>

<!DOCTYPE html>
<html lang="en-US">

<!-- Mirrored from ihk.8a1.myftpupload.com/metaverse-space/blog-page/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 29 Sep 2022 03:12:16 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="https://gmpg.org/xfn/11">

<title>Blog Page &#8211; Fellaz</title>
<meta name='robots' content='noindex, nofollow' />
<link rel='dns-prefetch' href='http://fonts.googleapis.com/' />
<link rel='dns-prefetch' href='http://s.w.org/' />
<link rel="alternate" type="application/rss+xml" title="Fellaz &raquo; Feed" href="https://ihk.8a1.myftpupload.com/feed/" />
<link rel="alternate" type="application/rss+xml" title="Fellaz &raquo; Comments Feed" href="https://ihk.8a1.myftpupload.com/comments/feed/" />
<link rel='preconnect' href='https://secureservercdn.net/' crossorigin />
<link rel="stylesheet" href="{{ asset('css/events.css') }}">
<script>
window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/14.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/14.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/secureservercdn.net\/198.71.190.114\/ihk.8a1.myftpupload.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=6.0.2&time=1664353118"}};
/*! This file is auto-generated */
!function(e,a,t){var n,r,o,i=a.createElement("canvas"),p=i.getContext&&i.getContext("2d");function s(e,t){var a=String.fromCharCode,e=(p.clearRect(0,0,i.width,i.height),p.fillText(a.apply(this,e),0,0),i.toDataURL());return p.clearRect(0,0,i.width,i.height),p.fillText(a.apply(this,t),0,0),e===i.toDataURL()}function c(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(o=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},r=0;r<o.length;r++)t.supports[o[r]]=function(e){if(!p||!p.fillText)return!1;switch(p.textBaseline="top",p.font="600 32px Arial",e){case"flag":return s([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])?!1:!s([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!s([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]);case"emoji":return!s([129777,127995,8205,129778,127999],[129777,127995,8203,129778,127999])}return!1}(o[r]),t.supports.everything=t.supports.everything&&t.supports[o[r]],"flag"!==o[r]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[o[r]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(e=t.source||{}).concatemoji?c(e.concatemoji):e.wpemoji&&e.twemoji&&(c(e.twemoji),c(e.wpemoji)))}(window,document,window._wpemojiSettings);
</script>
<style>
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 0.07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
	<link rel='stylesheet' id='astra-theme-css-css'  href='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/themes/astra/assets/css/minified/main.min.css?ver=3.9.1&amp;time=1664353118' media='all' />
<style id='astra-theme-css-inline-css'>
:root{--ast-container-default-xlg-padding:3em;--ast-container-default-lg-padding:3em;--ast-container-default-slg-padding:2em;--ast-container-default-md-padding:3em;--ast-container-default-sm-padding:3em;--ast-container-default-xs-padding:2.4em;--ast-container-default-xxs-padding:1.8em;}html{font-size:93.75%;}a{color:#fefefe;}a:hover,a:focus{color:#f2b8ff;}body,button,input,select,textarea,.ast-button,.ast-custom-button{font-family:'Open Sans',sans-serif;font-weight:inherit;font-size:15px;font-size:1rem;}blockquote{color:#b4b4b4;}p,.entry-content p{margin-bottom:0.5em;}h1,.entry-content h1,h2,.entry-content h2,h3,.entry-content h3,h4,.entry-content h4,h5,.entry-content h5,h6,.entry-content h6,.site-title,.site-title a{font-family:'Open Sans',sans-serif;font-weight:700;}.site-title{font-size:35px;font-size:2.3333333333333rem;display:block;}.ast-archive-description .ast-archive-title{font-size:40px;font-size:2.6666666666667rem;}.site-header .site-description{font-size:15px;font-size:1rem;display:none;}.entry-title{font-size:30px;font-size:2rem;}h1,.entry-content h1{font-size:40px;font-size:2.6666666666667rem;font-weight:700;font-family:'Open Sans',sans-serif;}h2,.entry-content h2{font-size:30px;font-size:2rem;font-weight:700;font-family:'Open Sans',sans-serif;}h3,.entry-content h3{font-size:25px;font-size:1.6666666666667rem;font-weight:700;font-family:'Open Sans',sans-serif;}h4,.entry-content h4{font-size:20px;font-size:1.3333333333333rem;font-weight:700;font-family:'Open Sans',sans-serif;}h5,.entry-content h5{font-size:18px;font-size:1.2rem;font-weight:700;font-family:'Open Sans',sans-serif;}h6,.entry-content h6{font-size:15px;font-size:1rem;font-weight:700;font-family:'Open Sans',sans-serif;}.ast-single-post .entry-title,.page-title{font-size:30px;font-size:2rem;}::selection{background-color:#f2b8ff;color:#000000;}body,h1,.entry-title a,.entry-content h1,h2,.entry-content h2,h3,.entry-content h3,h4,.entry-content h4,h5,.entry-content h5,h6,.entry-content h6{color:#ffffff;}.tagcloud a:hover,.tagcloud a:focus,.tagcloud a.current-item{color:#000000;border-color:#fefefe;background-color:#fefefe;}input:focus,input[type="text"]:focus,input[type="email"]:focus,input[type="url"]:focus,input[type="password"]:focus,input[type="reset"]:focus,input[type="search"]:focus,textarea:focus{border-color:#fefefe;}input[type="radio"]:checked,input[type=reset],input[type="checkbox"]:checked,input[type="checkbox"]:hover:checked,input[type="checkbox"]:focus:checked,input[type=range]::-webkit-slider-thumb{border-color:#fefefe;background-color:#fefefe;box-shadow:none;}.site-footer a:hover + .post-count,.site-footer a:focus + .post-count{background:#fefefe;border-color:#fefefe;}.single .nav-links .nav-previous,.single .nav-links .nav-next{color:#fefefe;}.entry-meta,.entry-meta *{line-height:1.45;color:#fefefe;}.entry-meta a:hover,.entry-meta a:hover *,.entry-meta a:focus,.entry-meta a:focus *,.page-links > .page-link,.page-links .page-link:hover,.post-navigation a:hover{color:#f2b8ff;}#cat option,.secondary .calendar_wrap thead a,.secondary .calendar_wrap thead a:visited{color:#fefefe;}.secondary .calendar_wrap #today,.ast-progress-val span{background:#fefefe;}.secondary a:hover + .post-count,.secondary a:focus + .post-count{background:#fefefe;border-color:#fefefe;}.calendar_wrap #today > a{color:#000000;}.page-links .page-link,.single .post-navigation a{color:#fefefe;}.ast-archive-title{color:#ffffff;}.widget-title{font-size:21px;font-size:1.4rem;color:#ffffff;}.ast-logo-title-inline .site-logo-img{padding-right:1em;}.ast-page-builder-template .hentry {margin: 0;}.ast-page-builder-template .site-content > .ast-container {max-width: 100%;padding: 0;}.ast-page-builder-template .site-content #primary {padding: 0;margin: 0;}.ast-page-builder-template .no-results {text-align: center;margin: 4em auto;}.ast-page-builder-template .ast-pagination {padding: 2em;}.ast-page-builder-template .entry-header.ast-no-title.ast-no-thumbnail {margin-top: 0;}.ast-page-builder-template .entry-header.ast-header-without-markup {margin-top: 0;margin-bottom: 0;}.ast-page-builder-template .entry-header.ast-no-title.ast-no-meta {margin-bottom: 0;}.ast-page-builder-template.single .post-navigation {padding-bottom: 2em;}.ast-page-builder-template.single-post .site-content > .ast-container {max-width: 100%;}.ast-page-builder-template .entry-header {margin-top: 2em;margin-left: auto;margin-right: auto;}.ast-page-builder-template .ast-archive-description {margin: 2em auto 0;padding-left: 20px;padding-right: 20px;}.ast-page-builder-template .ast-row {margin-left: 0;margin-right: 0;}.single.ast-page-builder-template .entry-header + .entry-content {margin-bottom: 2em;}@media(min-width: 921px) {.ast-page-builder-template.archive.ast-right-sidebar .ast-row article,.ast-page-builder-template.archive.ast-left-sidebar .ast-row article {padding-left: 0;padding-right: 0;}}@media (max-width:921px){#ast-desktop-header{display:none;}}@media (min-width:921px){#ast-mobile-header{display:none;}}.wp-block-buttons.aligncenter{justify-content:center;}@media (max-width:921px){.ast-theme-transparent-header #primary,.ast-theme-transparent-header #secondary{padding:0;}}@media (max-width:921px){.ast-plain-container.ast-no-sidebar #primary{padding:0;}}.ast-plain-container.ast-no-sidebar #primary{margin-top:0;margin-bottom:0;}@media (min-width:1200px){.ast-plain-container.ast-no-sidebar #primary{margin-top:60px;margin-bottom:60px;}}.wp-block-button.is-style-outline .wp-block-button__link{border-color:#4d14cd;border-top-width:0px;border-right-width:0px;border-bottom-width:0px;border-left-width:0px;}.wp-block-button.is-style-outline > .wp-block-button__link:not(.has-text-color),.wp-block-button.wp-block-button__link.is-style-outline:not(.has-text-color){color:#4d14cd;}.wp-block-button.is-style-outline .wp-block-button__link:hover,.wp-block-button.is-style-outline .wp-block-button__link:focus{color:#4d14cd !important;background-color:#ffffff;border-color:#ffffff;}.post-page-numbers.current .page-link,.ast-pagination .page-numbers.current{color:#000000;border-color:#f2b8ff;background-color:#f2b8ff;border-radius:2px;}.wp-block-button.is-style-outline .wp-block-button__link{border-top-width:0px;border-right-width:0px;border-bottom-width:0px;border-left-width:0px;}h1.widget-title{font-weight:700;}h2.widget-title{font-weight:700;}h3.widget-title{font-weight:700;}#page{display:flex;flex-direction:column;min-height:100vh;}.ast-404-layout-1 h1.page-title{color:var(--ast-global-color-2);}.single .post-navigation a{line-height:1em;height:inherit;}.error-404 .page-sub-title{font-size:1.5rem;font-weight:inherit;}.search .site-content .content-area .search-form{margin-bottom:0;}#page .site-content{flex-grow:1;}.widget{margin-bottom:3.5em;}#secondary li{line-height:1.5em;}#secondary .wp-block-group h2{margin-bottom:0.7em;}#secondary h2{font-size:1.7rem;}.ast-separate-container .ast-article-post,.ast-separate-container .ast-article-single,.ast-separate-container .ast-comment-list li.depth-1,.ast-separate-container .comment-respond{padding:3em;}.ast-separate-container .ast-comment-list li.depth-1,.hentry{margin-bottom:2em;}.ast-separate-container .ast-archive-description,.ast-separate-container .ast-author-box{background-color:var(--ast-global-color-5);border-bottom:1px solid var(--ast-border-color);}.ast-separate-container .comments-title{padding:2em 2em 0 2em;}.ast-page-builder-template .comment-form-textarea,.ast-comment-formwrap .ast-grid-common-col{padding:0;}.ast-comment-formwrap{padding:0 20px;display:inline-flex;column-gap:20px;}.archive.ast-page-builder-template .entry-header{margin-top:2em;}.ast-page-builder-template .ast-comment-formwrap{width:100%;}.entry-title{margin-bottom:0.5em;}.ast-archive-description .ast-archive-title{margin-bottom:10px;text-transform:capitalize;}.ast-archive-description p{font-size:inherit;font-weight:inherit;line-height:inherit;}@media (min-width:921px){.ast-left-sidebar.ast-page-builder-template #secondary,.archive.ast-right-sidebar.ast-page-builder-template .site-main{padding-left:20px;padding-right:20px;}}@media (max-width:544px){.ast-comment-formwrap.ast-row{column-gap:10px;}}@media (min-width:1201px){.ast-separate-container .ast-article-post,.ast-separate-container .ast-article-single,.ast-separate-container .ast-archive-description,.ast-separate-container .ast-author-box,.ast-separate-container .ast-404-layout-1,.ast-separate-container .no-results{padding:3em;}}@media (max-width:921px){.ast-separate-container #primary,.ast-separate-container #secondary{padding:1.5em 0;}#primary,#secondary{padding:1.5em 0;margin:0;}.ast-left-sidebar #content > .ast-container{display:flex;flex-direction:column-reverse;width:100%;}}@media (min-width:922px){.ast-separate-container.ast-right-sidebar #primary,.ast-separate-container.ast-left-sidebar #primary{border:0;}.search-no-results.ast-separate-container #primary{margin-bottom:4em;}}.wp-block-button .wp-block-button__link{color:#ffffff;}.wp-block-button .wp-block-button__link:hover,.wp-block-button .wp-block-button__link:focus{color:#4d14cd;background-color:#ffffff;border-color:#ffffff;}.wp-block-button .wp-block-button__link,.wp-block-search .wp-block-search__button,body .wp-block-file .wp-block-file__button{border-style:solid;border-top-width:0px;border-right-width:0px;border-left-width:0px;border-bottom-width:0px;border-color:#4d14cd;background-color:#4d14cd;color:#ffffff;font-family:inherit;font-weight:inherit;line-height:1;border-radius:30px;padding-top:10px;padding-right:20px;padding-bottom:10px;padding-left:20px;}.menu-toggle,button,.ast-button,.ast-custom-button,.button,input#submit,input[type="button"],input[type="submit"],input[type="reset"],form[CLASS*="wp-block-search__"].wp-block-search .wp-block-search__inside-wrapper .wp-block-search__button,body .wp-block-file .wp-block-file__button,.search .search-submit{border-style:solid;border-top-width:0px;border-right-width:0px;border-left-width:0px;border-bottom-width:0px;color:#ffffff;border-color:#4d14cd;background-color:#4d14cd;border-radius:30px;padding-top:10px;padding-right:20px;padding-bottom:10px;padding-left:20px;font-family:inherit;font-weight:inherit;line-height:1;}button:focus,.menu-toggle:hover,button:hover,.ast-button:hover,.ast-custom-button:hover .button:hover,.ast-custom-button:hover ,input[type=reset]:hover,input[type=reset]:focus,input#submit:hover,input#submit:focus,input[type="button"]:hover,input[type="button"]:focus,input[type="submit"]:hover,input[type="submit"]:focus,form[CLASS*="wp-block-search__"].wp-block-search .wp-block-search__inside-wrapper .wp-block-search__button:hover,form[CLASS*="wp-block-search__"].wp-block-search .wp-block-search__inside-wrapper .wp-block-search__button:focus,body .wp-block-file .wp-block-file__button:hover,body .wp-block-file .wp-block-file__button:focus{color:#4d14cd;background-color:#ffffff;border-color:#ffffff;}form[CLASS*="wp-block-search__"].wp-block-search .wp-block-search__inside-wrapper .wp-block-search__button.has-icon{padding-top:calc(10px - 3px);padding-right:calc(20px - 3px);padding-bottom:calc(10px - 3px);padding-left:calc(20px - 3px);}@media (min-width:544px){.ast-container{max-width:100%;}}@media (max-width:544px){.ast-separate-container .ast-article-post,.ast-separate-container .ast-article-single,.ast-separate-container .comments-title,.ast-separate-container .ast-archive-description{padding:1.5em 1em;}.ast-separate-container #content .ast-container{padding-left:0.54em;padding-right:0.54em;}.ast-separate-container .ast-comment-list li.depth-1{padding:1.5em 1em;margin-bottom:1.5em;}.ast-separate-container .ast-comment-list .bypostauthor{padding:.5em;}.ast-search-menu-icon.ast-dropdown-active .search-field{width:170px;}}@media (max-width:921px){.ast-mobile-header-stack .main-header-bar .ast-search-menu-icon{display:inline-block;}.ast-header-break-point.ast-header-custom-item-outside .ast-mobile-header-stack .main-header-bar .ast-search-icon{margin:0;}.ast-comment-avatar-wrap img{max-width:2.5em;}.ast-separate-container .ast-comment-list li.depth-1{padding:1.5em 2.14em;}.ast-separate-container .comment-respond{padding:2em 2.14em;}.ast-comment-meta{padding:0 1.8888em 1.3333em;}}.ast-separate-container{background-color:var(--ast-global-color-8);;background-image:none;;}@media (max-width:921px){.site-title{display:block;}.ast-archive-description .ast-archive-title{font-size:40px;}.site-header .site-description{display:none;}.entry-title{font-size:30px;}h1,.entry-content h1{font-size:30px;}h2,.entry-content h2{font-size:25px;}h3,.entry-content h3{font-size:20px;}.ast-single-post .entry-title,.page-title{font-size:30px;}}@media (max-width:544px){.site-title{display:block;}.ast-archive-description .ast-archive-title{font-size:40px;}.site-header .site-description{display:none;}.entry-title{font-size:30px;}h1,.entry-content h1{font-size:30px;}h2,.entry-content h2{font-size:25px;}h3,.entry-content h3{font-size:20px;}.ast-single-post .entry-title,.page-title{font-size:30px;}}@media (max-width:921px){html{font-size:85.5%;}}@media (max-width:544px){html{font-size:85.5%;}}@media (min-width:922px){.ast-container{max-width:1960px;}}@media (min-width:922px){.site-content .ast-container{display:flex;}}@media (max-width:921px){.site-content .ast-container{flex-direction:column;}}@media (min-width:922px){.main-header-menu .sub-menu .menu-item.ast-left-align-sub-menu:hover > .sub-menu,.main-header-menu .sub-menu .menu-item.ast-left-align-sub-menu.focus > .sub-menu{margin-left:-0px;}}blockquote,cite {font-style: initial;}.wp-block-file {display: flex;align-items: center;flex-wrap: wrap;justify-content: space-between;}.wp-block-pullquote {border: none;}.wp-block-pullquote blockquote::before {content: "\201D";font-family: "Helvetica",sans-serif;display: flex;transform: rotate( 180deg );font-size: 6rem;font-style: normal;line-height: 1;font-weight: bold;align-items: center;justify-content: center;}.has-text-align-right > blockquote::before {justify-content: flex-start;}.has-text-align-left > blockquote::before {justify-content: flex-end;}figure.wp-block-pullquote.is-style-solid-color blockquote {max-width: 100%;text-align: inherit;}html body {--wp--custom--ast-default-block-top-padding: 2em;--wp--custom--ast-default-block-right-padding: 2em;--wp--custom--ast-default-block-bottom-padding: 2em;--wp--custom--ast-default-block-left-padding: 2em;--wp--custom--ast-container-width: 1920px;--wp--custom--ast-content-width-size: 1920px;--wp--custom--ast-wide-width-size: calc(1920px + var(--wp--custom--ast-default-block-left-padding) + var(--wp--custom--ast-default-block-right-padding));}@media(max-width: 921px) {html body {--wp--custom--ast-default-block-top-padding: 2em;--wp--custom--ast-default-block-right-padding: 2em;--wp--custom--ast-default-block-bottom-padding: 2em;--wp--custom--ast-default-block-left-padding: 2em;}}@media(max-width: 544px) {html body {--wp--custom--ast-default-block-top-padding: 2em;--wp--custom--ast-default-block-right-padding: 2em;--wp--custom--ast-default-block-bottom-padding: 2em;--wp--custom--ast-default-block-left-padding: 2em;}}.entry-content > .wp-block-group,.entry-content > .wp-block-cover,.entry-content > .wp-block-columns {padding-top: var(--wp--custom--ast-default-block-top-padding);padding-right: var(--wp--custom--ast-default-block-right-padding);padding-bottom: var(--wp--custom--ast-default-block-bottom-padding);padding-left: var(--wp--custom--ast-default-block-left-padding);}.ast-plain-container.ast-no-sidebar .entry-content > .alignfull,.ast-page-builder-template .ast-no-sidebar .entry-content > .alignfull {margin-left: calc( -50vw + 50%);margin-right: calc( -50vw + 50%);max-width: 100vw;width: 100vw;}.ast-plain-container.ast-no-sidebar .entry-content .alignfull .alignfull,.ast-page-builder-template.ast-no-sidebar .entry-content .alignfull .alignfull,.ast-plain-container.ast-no-sidebar .entry-content .alignfull .alignwide,.ast-page-builder-template.ast-no-sidebar .entry-content .alignfull .alignwide,.ast-plain-container.ast-no-sidebar .entry-content .alignwide .alignfull,.ast-page-builder-template.ast-no-sidebar .entry-content .alignwide .alignfull,.ast-plain-container.ast-no-sidebar .entry-content .alignwide .alignwide,.ast-page-builder-template.ast-no-sidebar .entry-content .alignwide .alignwide,.ast-plain-container.ast-no-sidebar .entry-content .wp-block-column .alignfull,.ast-page-builder-template.ast-no-sidebar .entry-content .wp-block-column .alignfull,.ast-plain-container.ast-no-sidebar .entry-content .wp-block-column .alignwide,.ast-page-builder-template.ast-no-sidebar .entry-content .wp-block-column .alignwide {margin-left: auto;margin-right: auto;width: 100%;}[ast-blocks-layout] .wp-block-separator:not(.is-style-dots) {height: 0;}[ast-blocks-layout] .wp-block-separator {margin: 20px auto;}[ast-blocks-layout] .wp-block-separator:not(.is-style-wide):not(.is-style-dots) {max-width: 100px;}[ast-blocks-layout] .wp-block-separator.has-background {padding: 0;}.entry-content[ast-blocks-layout] > * {max-width: var(--wp--custom--ast-content-width-size);margin-left: auto;margin-right: auto;}.entry-content[ast-blocks-layout] > .alignwide {max-width: var(--wp--custom--ast-wide-width-size);}.entry-content[ast-blocks-layout] .alignfull {max-width: none;}.entry-content .wp-block-columns {margin-bottom: 0;}blockquote {margin: 1.5em;border: none;}.wp-block-quote:not(.has-text-align-right):not(.has-text-align-center) {border-left: 5px solid rgba(0,0,0,0.05);}.has-text-align-right > blockquote,blockquote.has-text-align-right {border-right: 5px solid rgba(0,0,0,0.05);}.has-text-align-left > blockquote,blockquote.has-text-align-left {border-left: 5px solid rgba(0,0,0,0.05);}.wp-block-site-tagline,.wp-block-latest-posts .read-more {margin-top: 15px;}.wp-block-loginout p label {display: block;}.wp-block-loginout p:not(.login-remember):not(.login-submit) input {width: 100%;}.wp-block-loginout input:focus {border-color: transparent;}.wp-block-loginout input:focus {outline: thin dotted;}.entry-content .wp-block-media-text .wp-block-media-text__content {padding: 0 0 0 8%;}.entry-content .wp-block-media-text.has-media-on-the-right .wp-block-media-text__content {padding: 0 8% 0 0;}.entry-content .wp-block-media-text.has-background .wp-block-media-text__content {padding: 8%;}.entry-content .wp-block-cover:not([class*="background-color"]) .wp-block-cover__inner-container,.entry-content .wp-block-cover:not([class*="background-color"]) .wp-block-cover-image-text,.entry-content .wp-block-cover:not([class*="background-color"]) .wp-block-cover-text,.entry-content .wp-block-cover-image:not([class*="background-color"]) .wp-block-cover__inner-container,.entry-content .wp-block-cover-image:not([class*="background-color"]) .wp-block-cover-image-text,.entry-content .wp-block-cover-image:not([class*="background-color"]) .wp-block-cover-text {color: var(--ast-global-color-5);}.wp-block-loginout .login-remember input {width: 1.1rem;height: 1.1rem;margin: 0 5px 4px 0;vertical-align: middle;}.wp-block-latest-posts > li > *:first-child,.wp-block-latest-posts:not(.is-grid) > li:first-child {margin-top: 0;}.wp-block-search__inside-wrapper .wp-block-search__input {padding: 0 10px;color: var(--ast-global-color-3);background: var(--ast-global-color-5);border-color: var(--ast-border-color);}.wp-block-latest-posts .read-more {margin-bottom: 1.5em;}.wp-block-search__no-button .wp-block-search__inside-wrapper .wp-block-search__input {padding-top: 5px;padding-bottom: 5px;}.wp-block-latest-posts .wp-block-latest-posts__post-date,.wp-block-latest-posts .wp-block-latest-posts__post-author {font-size: 1rem;}.wp-block-latest-posts > li > *,.wp-block-latest-posts:not(.is-grid) > li {margin-top: 12px;margin-bottom: 12px;}.ast-page-builder-template .entry-content[ast-blocks-layout] > *,.ast-page-builder-template .entry-content[ast-blocks-layout] > .alignfull > * {max-width: none;}.ast-page-builder-template .entry-content[ast-blocks-layout] > .alignwide > * {max-width: var(--wp--custom--ast-wide-width-size);}.ast-page-builder-template .entry-content[ast-blocks-layout] > .inherit-container-width > *,.ast-page-builder-template .entry-content[ast-blocks-layout] > * > *,.entry-content[ast-blocks-layout] > .wp-block-cover .wp-block-cover__inner-container {max-width: var(--wp--custom--ast-content-width-size);margin-left: auto;margin-right: auto;}.entry-content[ast-blocks-layout] .wp-block-cover:not(.alignleft):not(.alignright) {width: auto;}@media(max-width: 1200px) {.ast-separate-container .entry-content > .alignfull,.ast-separate-container .entry-content[ast-blocks-layout] > .alignwide,.ast-plain-container .entry-content[ast-blocks-layout] > .alignwide,.ast-plain-container .entry-content .alignfull {margin-left: calc(-1 * min(var(--ast-container-default-xlg-padding),20px)) ;margin-right: calc(-1 * min(var(--ast-container-default-xlg-padding),20px));}}@media(min-width: 1201px) {.ast-separate-container .entry-content > .alignfull {margin-left: calc(-1 * var(--ast-container-default-xlg-padding) );margin-right: calc(-1 * var(--ast-container-default-xlg-padding) );}.ast-separate-container .entry-content[ast-blocks-layout] > .alignwide,.ast-plain-container .entry-content[ast-blocks-layout] > .alignwide {margin-left: calc(-1 * var(--wp--custom--ast-default-block-left-padding) );margin-right: calc(-1 * var(--wp--custom--ast-default-block-right-padding) );}}@media(min-width: 921px) {.ast-separate-container .entry-content .wp-block-group.alignwide:not(.inherit-container-width) > :where(:not(.alignleft):not(.alignright)),.ast-plain-container .entry-content .wp-block-group.alignwide:not(.inherit-container-width) > :where(:not(.alignleft):not(.alignright)) {max-width: calc( var(--wp--custom--ast-content-width-size) + 80px );}.ast-plain-container.ast-right-sidebar .entry-content[ast-blocks-layout] .alignfull,.ast-plain-container.ast-left-sidebar .entry-content[ast-blocks-layout] .alignfull {margin-left: -60px;margin-right: -60px;}}@media(min-width: 544px) {.entry-content > .alignleft {margin-right: 20px;}.entry-content > .alignright {margin-left: 20px;}}@media (max-width:544px){.wp-block-columns .wp-block-column:not(:last-child){margin-bottom:20px;}.wp-block-latest-posts{margin:0;}}@media( max-width: 600px ) {.entry-content .wp-block-media-text .wp-block-media-text__content,.entry-content .wp-block-media-text.has-media-on-the-right .wp-block-media-text__content {padding: 8% 0 0;}.entry-content .wp-block-media-text.has-background .wp-block-media-text__content {padding: 8%;}}:root .has-ast-global-color-0-color{color:var(--ast-global-color-0);}:root .has-ast-global-color-0-background-color{background-color:var(--ast-global-color-0);}:root .wp-block-button .has-ast-global-color-0-color{color:var(--ast-global-color-0);}:root .wp-block-button .has-ast-global-color-0-background-color{background-color:var(--ast-global-color-0);}:root .has-ast-global-color-1-color{color:var(--ast-global-color-1);}:root .has-ast-global-color-1-background-color{background-color:var(--ast-global-color-1);}:root .wp-block-button .has-ast-global-color-1-color{color:var(--ast-global-color-1);}:root .wp-block-button .has-ast-global-color-1-background-color{background-color:var(--ast-global-color-1);}:root .has-ast-global-color-2-color{color:var(--ast-global-color-2);}:root .has-ast-global-color-2-background-color{background-color:var(--ast-global-color-2);}:root .wp-block-button .has-ast-global-color-2-color{color:var(--ast-global-color-2);}:root .wp-block-button .has-ast-global-color-2-background-color{background-color:var(--ast-global-color-2);}:root .has-ast-global-color-3-color{color:var(--ast-global-color-3);}:root .has-ast-global-color-3-background-color{background-color:var(--ast-global-color-3);}:root .wp-block-button .has-ast-global-color-3-color{color:var(--ast-global-color-3);}:root .wp-block-button .has-ast-global-color-3-background-color{background-color:var(--ast-global-color-3);}:root .has-ast-global-color-4-color{color:var(--ast-global-color-4);}:root .has-ast-global-color-4-background-color{background-color:var(--ast-global-color-4);}:root .wp-block-button .has-ast-global-color-4-color{color:var(--ast-global-color-4);}:root .wp-block-button .has-ast-global-color-4-background-color{background-color:var(--ast-global-color-4);}:root .has-ast-global-color-5-color{color:var(--ast-global-color-5);}:root .has-ast-global-color-5-background-color{background-color:var(--ast-global-color-5);}:root .wp-block-button .has-ast-global-color-5-color{color:var(--ast-global-color-5);}:root .wp-block-button .has-ast-global-color-5-background-color{background-color:var(--ast-global-color-5);}:root .has-ast-global-color-6-color{color:var(--ast-global-color-6);}:root .has-ast-global-color-6-background-color{background-color:var(--ast-global-color-6);}:root .wp-block-button .has-ast-global-color-6-color{color:var(--ast-global-color-6);}:root .wp-block-button .has-ast-global-color-6-background-color{background-color:var(--ast-global-color-6);}:root .has-ast-global-color-7-color{color:var(--ast-global-color-7);}:root .has-ast-global-color-7-background-color{background-color:var(--ast-global-color-7);}:root .wp-block-button .has-ast-global-color-7-color{color:var(--ast-global-color-7);}:root .wp-block-button .has-ast-global-color-7-background-color{background-color:var(--ast-global-color-7);}:root .has-ast-global-color-8-color{color:var(--ast-global-color-8);}:root .has-ast-global-color-8-background-color{background-color:var(--ast-global-color-8);}:root .wp-block-button .has-ast-global-color-8-color{color:var(--ast-global-color-8);}:root .wp-block-button .has-ast-global-color-8-background-color{background-color:var(--ast-global-color-8);}:root{--ast-global-color-0:#0170B9;--ast-global-color-1:#3a3a3a;--ast-global-color-2:#3a3a3a;--ast-global-color-3:#4B4F58;--ast-global-color-4:#F5F5F5;--ast-global-color-5:#FFFFFF;--ast-global-color-6:#E5E5E5;--ast-global-color-7:#424242;--ast-global-color-8:#000000;}:root {--ast-border-color : var(--ast-global-color-6);}.ast-breadcrumbs .trail-browse,.ast-breadcrumbs .trail-items,.ast-breadcrumbs .trail-items li{display:inline-block;margin:0;padding:0;border:none;background:inherit;text-indent:0;}.ast-breadcrumbs .trail-browse{font-size:inherit;font-style:inherit;font-weight:inherit;color:inherit;}.ast-breadcrumbs .trail-items{list-style:none;}.trail-items li::after{padding:0 0.3em;content:"\00bb";}.trail-items li:last-of-type::after{display:none;}h1,.entry-content h1,h2,.entry-content h2,h3,.entry-content h3,h4,.entry-content h4,h5,.entry-content h5,h6,.entry-content h6{color:#ffffff;}.entry-title a{color:#ffffff;}@media (max-width:921px){.ast-builder-grid-row-container.ast-builder-grid-row-tablet-3-firstrow .ast-builder-grid-row > *:first-child,.ast-builder-grid-row-container.ast-builder-grid-row-tablet-3-lastrow .ast-builder-grid-row > *:last-child{grid-column:1 / -1;}}@media (max-width:544px){.ast-builder-grid-row-container.ast-builder-grid-row-mobile-3-firstrow .ast-builder-grid-row > *:first-child,.ast-builder-grid-row-container.ast-builder-grid-row-mobile-3-lastrow .ast-builder-grid-row > *:last-child{grid-column:1 / -1;}}.ast-builder-layout-element[data-section="title_tagline"]{display:flex;}@media (max-width:921px){.ast-header-break-point .ast-builder-layout-element[data-section="title_tagline"]{display:flex;}}@media (max-width:544px){.ast-header-break-point .ast-builder-layout-element[data-section="title_tagline"]{display:flex;}}.ast-builder-menu-1{font-family:inherit;font-weight:inherit;}.ast-builder-menu-1 .sub-menu,.ast-builder-menu-1 .inline-on-mobile .sub-menu{border-top-width:2px;border-bottom-width:0px;border-right-width:0px;border-left-width:0px;border-color:#f2b8ff;border-style:solid;border-radius:0px;}.ast-builder-menu-1 .main-header-menu > .menu-item > .sub-menu,.ast-builder-menu-1 .main-header-menu > .menu-item > .astra-full-megamenu-wrapper{margin-top:0px;}.ast-desktop .ast-builder-menu-1 .main-header-menu > .menu-item > .sub-menu:before,.ast-desktop .ast-builder-menu-1 .main-header-menu > .menu-item > .astra-full-megamenu-wrapper:before{height:calc( 0px + 5px );}.ast-desktop .ast-builder-menu-1 .menu-item .sub-menu .menu-link{border-style:none;}@media (max-width:921px){.ast-header-break-point .ast-builder-menu-1 .menu-item.menu-item-has-children > .ast-menu-toggle{top:0;}.ast-builder-menu-1 .menu-item-has-children > .menu-link:after{content:unset;}}@media (max-width:544px){.ast-header-break-point .ast-builder-menu-1 .menu-item.menu-item-has-children > .ast-menu-toggle{top:0;}}.ast-builder-menu-1{display:flex;}@media (max-width:921px){.ast-header-break-point .ast-builder-menu-1{display:flex;}}@media (max-width:544px){.ast-header-break-point .ast-builder-menu-1{display:flex;}}.site-below-footer-wrap{padding-top:20px;padding-bottom:20px;}.site-below-footer-wrap[data-section="section-below-footer-builder"]{background-color:#eeeeee;;min-height:80px;}.site-below-footer-wrap[data-section="section-below-footer-builder"] .ast-builder-grid-row{max-width:1920px;margin-left:auto;margin-right:auto;}.site-below-footer-wrap[data-section="section-below-footer-builder"] .ast-builder-grid-row,.site-below-footer-wrap[data-section="section-below-footer-builder"] .site-footer-section{align-items:flex-start;}.site-below-footer-wrap[data-section="section-below-footer-builder"].ast-footer-row-inline .site-footer-section{display:flex;margin-bottom:0;}.ast-builder-grid-row-full .ast-builder-grid-row{grid-template-columns:1fr;}@media (max-width:921px){.site-below-footer-wrap[data-section="section-below-footer-builder"].ast-footer-row-tablet-inline .site-footer-section{display:flex;margin-bottom:0;}.site-below-footer-wrap[data-section="section-below-footer-builder"].ast-footer-row-tablet-stack .site-footer-section{display:block;margin-bottom:10px;}.ast-builder-grid-row-container.ast-builder-grid-row-tablet-full .ast-builder-grid-row{grid-template-columns:1fr;}}@media (max-width:544px){.site-below-footer-wrap[data-section="section-below-footer-builder"].ast-footer-row-mobile-inline .site-footer-section{display:flex;margin-bottom:0;}.site-below-footer-wrap[data-section="section-below-footer-builder"].ast-footer-row-mobile-stack .site-footer-section{display:block;margin-bottom:10px;}.ast-builder-grid-row-container.ast-builder-grid-row-mobile-full .ast-builder-grid-row{grid-template-columns:1fr;}}.site-below-footer-wrap[data-section="section-below-footer-builder"]{display:grid;}@media (max-width:921px){.ast-header-break-point .site-below-footer-wrap[data-section="section-below-footer-builder"]{display:grid;}}@media (max-width:544px){.ast-header-break-point .site-below-footer-wrap[data-section="section-below-footer-builder"]{display:grid;}}.ast-footer-copyright{text-align:center;}.ast-footer-copyright {color:#ffffff;}@media (max-width:921px){.ast-footer-copyright{text-align:center;}}@media (max-width:544px){.ast-footer-copyright{text-align:center;}}.ast-footer-copyright.ast-builder-layout-element{display:flex;}@media (max-width:921px){.ast-header-break-point .ast-footer-copyright.ast-builder-layout-element{display:flex;}}@media (max-width:544px){.ast-header-break-point .ast-footer-copyright.ast-builder-layout-element{display:flex;}}.elementor-widget-heading .elementor-heading-title{margin:0;}.elementor-post.elementor-grid-item.hentry{margin-bottom:0;}.woocommerce div.product .elementor-element.elementor-products-grid .related.products ul.products li.product,.elementor-element .elementor-wc-products .woocommerce[class*='columns-'] ul.products li.product{width:auto;margin:0;float:none;}.elementor-toc__list-wrapper{margin:0;}.ast-left-sidebar .elementor-section.elementor-section-stretched,.ast-right-sidebar .elementor-section.elementor-section-stretched{max-width:100%;left:0 !important;}.elementor-template-full-width .ast-container{display:block;}@media (max-width:544px){.elementor-element .elementor-wc-products .woocommerce[class*="columns-"] ul.products li.product{width:auto;margin:0;}.elementor-element .woocommerce .woocommerce-result-count{float:none;}}.ast-header-break-point .main-header-bar{border-bottom-width:1px;}@media (min-width:922px){.main-header-bar{border-bottom-width:1px;}}.main-header-menu .menu-item, #astra-footer-menu .menu-item, .main-header-bar .ast-masthead-custom-menu-items{-js-display:flex;display:flex;-webkit-box-pack:center;-webkit-justify-content:center;-moz-box-pack:center;-ms-flex-pack:center;justify-content:center;-webkit-box-orient:vertical;-webkit-box-direction:normal;-webkit-flex-direction:column;-moz-box-orient:vertical;-moz-box-direction:normal;-ms-flex-direction:column;flex-direction:column;}.main-header-menu > .menu-item > .menu-link, #astra-footer-menu > .menu-item > .menu-link{height:100%;-webkit-box-align:center;-webkit-align-items:center;-moz-box-align:center;-ms-flex-align:center;align-items:center;-js-display:flex;display:flex;}.ast-header-break-point .main-navigation ul .menu-item .menu-link .icon-arrow:first-of-type svg{top:.2em;margin-top:0px;margin-left:0px;width:.65em;transform:translate(0, -2px) rotateZ(270deg);}.ast-mobile-popup-content .ast-submenu-expanded > .ast-menu-toggle{transform:rotateX(180deg);}.ast-separate-container .blog-layout-1, .ast-separate-container .blog-layout-2, .ast-separate-container .blog-layout-3{background-color:transparent;background-image:none;}.ast-separate-container .ast-article-post{background-color:var(--ast-global-color-8);;background-image:none;;}@media (max-width:921px){.ast-separate-container .ast-article-post{background-color:var(--ast-global-color-5);;background-image:none;;}}@media (max-width:544px){.ast-separate-container .ast-article-post{background-color:var(--ast-global-color-5);;background-image:none;;}}.ast-separate-container .ast-article-single:not(.ast-related-post), .ast-separate-container .comments-area .comment-respond,.ast-separate-container .comments-area .ast-comment-list li, .ast-separate-container .ast-woocommerce-container, .ast-separate-container .error-404, .ast-separate-container .no-results, .single.ast-separate-container .site-main .ast-author-meta, .ast-separate-container .related-posts-title-wrapper, .ast-separate-container.ast-two-container #secondary .widget,.ast-separate-container .comments-count-wrapper, .ast-box-layout.ast-plain-container .site-content,.ast-padded-layout.ast-plain-container .site-content, .ast-separate-container .comments-area .comments-title{background-color:var(--ast-global-color-8);;background-image:none;;}@media (max-width:921px){.ast-separate-container .ast-article-single:not(.ast-related-post), .ast-separate-container .comments-area .comment-respond,.ast-separate-container .comments-area .ast-comment-list li, .ast-separate-container .ast-woocommerce-container, .ast-separate-container .error-404, .ast-separate-container .no-results, .single.ast-separate-container .site-main .ast-author-meta, .ast-separate-container .related-posts-title-wrapper, .ast-separate-container.ast-two-container #secondary .widget,.ast-separate-container .comments-count-wrapper, .ast-box-layout.ast-plain-container .site-content,.ast-padded-layout.ast-plain-container .site-content, .ast-separate-container .comments-area .comments-title{background-color:var(--ast-global-color-5);;background-image:none;;}}@media (max-width:544px){.ast-separate-container .ast-article-single:not(.ast-related-post), .ast-separate-container .comments-area .comment-respond,.ast-separate-container .comments-area .ast-comment-list li, .ast-separate-container .ast-woocommerce-container, .ast-separate-container .error-404, .ast-separate-container .no-results, .single.ast-separate-container .site-main .ast-author-meta, .ast-separate-container .related-posts-title-wrapper, .ast-separate-container.ast-two-container #secondary .widget,.ast-separate-container .comments-count-wrapper, .ast-box-layout.ast-plain-container .site-content,.ast-padded-layout.ast-plain-container .site-content, .ast-separate-container .comments-area .comments-title{background-color:var(--ast-global-color-5);;background-image:none;;}}.ast-plain-container, .ast-page-builder-template{background-color:var(--ast-global-color-8);;background-image:none;;}@media (max-width:921px){.ast-plain-container, .ast-page-builder-template{background-color:var(--ast-global-color-5);;background-image:none;;}}@media (max-width:544px){.ast-plain-container, .ast-page-builder-template{background-color:var(--ast-global-color-5);;background-image:none;;}}.ast-mobile-header-content > *,.ast-desktop-header-content > * {padding: 10px 0;height: auto;}.ast-mobile-header-content > *:first-child,.ast-desktop-header-content > *:first-child {padding-top: 10px;}.ast-mobile-header-content > .ast-builder-menu,.ast-desktop-header-content > .ast-builder-menu {padding-top: 0;}.ast-mobile-header-content > *:last-child,.ast-desktop-header-content > *:last-child {padding-bottom: 0;}.ast-mobile-header-content .ast-search-menu-icon.ast-inline-search label,.ast-desktop-header-content .ast-search-menu-icon.ast-inline-search label {width: 100%;}.ast-desktop-header-content .main-header-bar-navigation .ast-submenu-expanded > .ast-menu-toggle::before {transform: rotateX(180deg);}#ast-desktop-header .ast-desktop-header-content,.ast-mobile-header-content .ast-search-icon,.ast-desktop-header-content .ast-search-icon,.ast-mobile-header-wrap .ast-mobile-header-content,.ast-main-header-nav-open.ast-popup-nav-open .ast-mobile-header-wrap .ast-mobile-header-content,.ast-main-header-nav-open.ast-popup-nav-open .ast-desktop-header-content {display: none;}.ast-main-header-nav-open.ast-header-break-point #ast-desktop-header .ast-desktop-header-content,.ast-main-header-nav-open.ast-header-break-point .ast-mobile-header-wrap .ast-mobile-header-content {display: block;}.ast-desktop .ast-desktop-header-content .astra-menu-animation-slide-up > .menu-item > .sub-menu,.ast-desktop .ast-desktop-header-content .astra-menu-animation-slide-up > .menu-item .menu-item > .sub-menu,.ast-desktop .ast-desktop-header-content .astra-menu-animation-slide-down > .menu-item > .sub-menu,.ast-desktop .ast-desktop-header-content .astra-menu-animation-slide-down > .menu-item .menu-item > .sub-menu,.ast-desktop .ast-desktop-header-content .astra-menu-animation-fade > .menu-item > .sub-menu,.ast-desktop .ast-desktop-header-content .astra-menu-animation-fade > .menu-item .menu-item > .sub-menu {opacity: 1;visibility: visible;}.ast-hfb-header.ast-default-menu-enable.ast-header-break-point .ast-mobile-header-wrap .ast-mobile-header-content .main-header-bar-navigation {width: unset;margin: unset;}.ast-mobile-header-content.content-align-flex-end .main-header-bar-navigation .menu-item-has-children > .ast-menu-toggle,.ast-desktop-header-content.content-align-flex-end .main-header-bar-navigation .menu-item-has-children > .ast-menu-toggle {left: calc( 20px - 0.907em);}.ast-mobile-header-content .ast-search-menu-icon,.ast-mobile-header-content .ast-search-menu-icon.slide-search,.ast-desktop-header-content .ast-search-menu-icon,.ast-desktop-header-content .ast-search-menu-icon.slide-search {width: 100%;position: relative;display: block;right: auto;transform: none;}.ast-mobile-header-content .ast-search-menu-icon.slide-search .search-form,.ast-mobile-header-content .ast-search-menu-icon .search-form,.ast-desktop-header-content .ast-search-menu-icon.slide-search .search-form,.ast-desktop-header-content .ast-search-menu-icon .search-form {right: 0;visibility: visible;opacity: 1;position: relative;top: auto;transform: none;padding: 0;display: block;overflow: hidden;}.ast-mobile-header-content .ast-search-menu-icon.ast-inline-search .search-field,.ast-mobile-header-content .ast-search-menu-icon .search-field,.ast-desktop-header-content .ast-search-menu-icon.ast-inline-search .search-field,.ast-desktop-header-content .ast-search-menu-icon .search-field {width: 100%;padding-right: 5.5em;}.ast-mobile-header-content .ast-search-menu-icon .search-submit,.ast-desktop-header-content .ast-search-menu-icon .search-submit {display: block;position: absolute;height: 100%;top: 0;right: 0;padding: 0 1em;border-radius: 0;}.ast-hfb-header.ast-default-menu-enable.ast-header-break-point .ast-mobile-header-wrap .ast-mobile-header-content .main-header-bar-navigation ul .sub-menu .menu-link {padding-left: 30px;}.ast-hfb-header.ast-default-menu-enable.ast-header-break-point .ast-mobile-header-wrap .ast-mobile-header-content .main-header-bar-navigation .sub-menu .menu-item .menu-item .menu-link {padding-left: 40px;}.ast-mobile-popup-drawer.active .ast-mobile-popup-inner{background-color:#ffffff;;}.ast-mobile-header-wrap .ast-mobile-header-content, .ast-desktop-header-content{background-color:#ffffff;;}.ast-mobile-popup-content > *, .ast-mobile-header-content > *, .ast-desktop-popup-content > *, .ast-desktop-header-content > *{padding-top:0px;padding-bottom:0px;}.content-align-flex-start .ast-builder-layout-element{justify-content:flex-start;}.content-align-flex-start .main-header-menu{text-align:left;}.ast-mobile-popup-drawer.active .menu-toggle-close{color:#3a3a3a;}.ast-mobile-header-wrap .ast-primary-header-bar,.ast-primary-header-bar .site-primary-header-wrap{min-height:80px;}.ast-desktop .ast-primary-header-bar .main-header-menu > .menu-item{line-height:80px;}@media (max-width:921px){#masthead .ast-mobile-header-wrap .ast-primary-header-bar,#masthead .ast-mobile-header-wrap .ast-below-header-bar{padding-left:20px;padding-right:20px;}}.ast-header-break-point .ast-primary-header-bar{border-bottom-width:1px;border-bottom-color:#eaeaea;border-bottom-style:solid;}@media (min-width:922px){.ast-primary-header-bar{border-bottom-width:1px;border-bottom-color:#eaeaea;border-bottom-style:solid;}}.ast-primary-header-bar{background-color:#ffffff;;}.ast-primary-header-bar{display:block;}@media (max-width:921px){.ast-header-break-point .ast-primary-header-bar{display:grid;}}@media (max-width:544px){.ast-header-break-point .ast-primary-header-bar{display:grid;}}[data-section="section-header-mobile-trigger"] .ast-button-wrap .ast-mobile-menu-trigger-minimal{color:#f2b8ff;border:none;background:transparent;}[data-section="section-header-mobile-trigger"] .ast-button-wrap .mobile-menu-toggle-icon .ast-mobile-svg{width:20px;height:20px;fill:#f2b8ff;}[data-section="section-header-mobile-trigger"] .ast-button-wrap .mobile-menu-wrap .mobile-menu{color:#f2b8ff;}.ast-builder-menu-mobile .main-navigation .menu-item > .menu-link{font-family:inherit;font-weight:inherit;}.ast-builder-menu-mobile .main-navigation .menu-item.menu-item-has-children > .ast-menu-toggle{top:0;}.ast-builder-menu-mobile .main-navigation .menu-item-has-children > .menu-link:after{content:unset;}.ast-hfb-header .ast-builder-menu-mobile .main-header-menu, .ast-hfb-header .ast-builder-menu-mobile .main-navigation .menu-item .menu-link, .ast-hfb-header .ast-builder-menu-mobile .main-navigation .menu-item .sub-menu .menu-link{border-style:none;}.ast-builder-menu-mobile .main-navigation .menu-item.menu-item-has-children > .ast-menu-toggle{top:0;}@media (max-width:921px){.ast-builder-menu-mobile .main-navigation .menu-item.menu-item-has-children > .ast-menu-toggle{top:0;}.ast-builder-menu-mobile .main-navigation .menu-item-has-children > .menu-link:after{content:unset;}}@media (max-width:544px){.ast-builder-menu-mobile .main-navigation .menu-item.menu-item-has-children > .ast-menu-toggle{top:0;}}.ast-builder-menu-mobile .main-navigation{display:block;}@media (max-width:921px){.ast-header-break-point .ast-builder-menu-mobile .main-navigation{display:block;}}@media (max-width:544px){.ast-header-break-point .ast-builder-menu-mobile .main-navigation{display:block;}}:root{--e-global-color-astglobalcolor0:#0170B9;--e-global-color-astglobalcolor1:#3a3a3a;--e-global-color-astglobalcolor2:#3a3a3a;--e-global-color-astglobalcolor3:#4B4F58;--e-global-color-astglobalcolor4:#F5F5F5;--e-global-color-astglobalcolor5:#FFFFFF;--e-global-color-astglobalcolor6:#E5E5E5;--e-global-color-astglobalcolor7:#424242;--e-global-color-astglobalcolor8:#000000;}
</style>
<link rel='stylesheet' id='astra-google-fonts-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A400%2C700%2C&amp;display=fallback&amp;ver=3.9.1' media='all' />
<link rel='stylesheet' id='pa-frontend-css'  href='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/premium-addons-elementor/pa-frontend-471e26a27.min.css?ver=1664421135&amp;time=1664353118' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.6.3&amp;time=1664353118' media='all' />
<link rel='stylesheet' id='astra-contact-form-7-css'  href='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/themes/astra/assets/css/minified/compatibility/contact-form-7-main.min.css?ver=3.9.1&amp;time=1664353118' media='all' />
<link rel='stylesheet' id='dashicons-css'  href='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-includes/css/dashicons.min.css?ver=6.0.2&amp;time=1664353118' media='all' />
<link rel='stylesheet' id='eleganticons-css'  href='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/wp-menu-icons/icons/eleganticons/style.min.css?ver=6.0.2&amp;time=1664353118' media='all' />
<link rel='stylesheet' id='wpmi-icons-css'  href='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/wp-menu-icons/assets/css/wpmi.css?ver=2.1.9&amp;time=1664353118' media='all' />
<link rel='stylesheet' id='wpfront-scroll-top-css'  href='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/wpfront-scroll-top/css/wpfront-scroll-top.min.css?ver=2.0.7.08086&amp;time=1664353118' media='all' />
<link rel='stylesheet' id='wp-components-css'  href='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-includes/css/dist/components/style.min.css?ver=6.0.2&amp;time=1664353118' media='all' />
<link rel='stylesheet' id='godaddy-styles-css'  href='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/coblocks/includes/Dependencies/GoDaddy/Styles/build/latest.css?ver=0.4.2&amp;time=1664353118' media='all' />
<link rel='stylesheet' id='hfe-style-css'  href='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/header-footer-elementor/assets/css/header-footer-elementor.css?ver=1.6.13&amp;time=1664353118' media='all' />
<link rel='stylesheet' id='elementor-icons-css'  href='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css?ver=5.16.0&amp;time=1664353118' media='all' />
<link rel='stylesheet' id='elementor-frontend-css'  href='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/elementor/assets/css/frontend-lite.min.css?ver=3.7.3&amp;time=1664353118' media='all' />
<link rel='stylesheet' id='elementor-post-7-css'  href='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/elementor/css/post-7.css?ver=1664352893&amp;time=1664353118' media='all' />
<link rel='stylesheet' id='elementor-global-css'  href='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/elementor/css/global.css?ver=1664352894&amp;time=1664353118' media='all' />
<link rel='stylesheet' id='elementor-post-1093-css'  href='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/elementor/css/post-1093.css?ver=1664421063&amp;time=1664353118' media='all' />
<link rel='stylesheet' id='hfe-widgets-style-css'  href='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/header-footer-elementor/inc/widgets-css/frontend.css?ver=1.6.13&amp;time=1664353118' media='all' />
<link rel='stylesheet' id='elementor-post-1010-css'  href='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/elementor/css/post-1010.css?ver=1664352894&amp;time=1664353118' media='all' />
<link rel='stylesheet' id='elementor-post-1063-css'  href='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/elementor/css/post-1063.css?ver=1664352894&amp;time=1664353118' media='all' />
<link rel='stylesheet' id='google-fonts-1-css'  href='https://fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7COpen+Sans%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&amp;display=auto&amp;ver=6.0.2' media='all' />
<link rel='stylesheet' id='elementor-icons-shared-0-css'  href='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/elementor/assets/lib/font-awesome/css/fontawesome.min.css?ver=5.15.3&amp;time=1664353118' media='all' />
<link rel='stylesheet' id='elementor-icons-fa-solid-css'  href='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/elementor/assets/lib/font-awesome/css/solid.min.css?ver=5.15.3&amp;time=1664353118' media='all' />
<link rel='stylesheet' id='elementor-icons-fa-regular-css'  href='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/elementor/assets/lib/font-awesome/css/regular.min.css?ver=5.15.3&amp;time=1664353118' media='all' />
<link rel='stylesheet' id='elementor-icons-fa-brands-css'  href='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/elementor/assets/lib/font-awesome/css/brands.min.css?ver=5.15.3&amp;time=1664353118' media='all' />
<!--[if IE]>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/themes/astra/assets/js/minified/flexibility.min.js?ver=3.9.1&#038;time=1664353118' id='astra-flexibility-js'></script>
<script id='astra-flexibility-js-after'>
flexibility(document.documentElement);
</script>
<![endif]-->
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-includes/js/jquery/jquery.min.js?ver=3.6.0&amp;time=1664353118' id='jquery-core-js'></script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2&amp;time=1664353118' id='jquery-migrate-js'></script>
<link rel="https://api.w.org/" href="https://ihk.8a1.myftpupload.com/wp-json/" /><link rel="alternate" type="application/json" href="https://ihk.8a1.myftpupload.com/wp-json/wp/v2/pages/1093" /><link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://ihk.8a1.myftpupload.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-includes/wlwmanifest.xml?time=1664353118" /> 
<link rel="canonical" href="https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/" />
<link rel='shortlink' href='https://ihk.8a1.myftpupload.com/?p=1093' />
<link rel="alternate" type="application/json+oembed" href="https://ihk.8a1.myftpupload.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fihk.8a1.myftpupload.com%2Fmetaverse-space%2Fblog-page%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://ihk.8a1.myftpupload.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fihk.8a1.myftpupload.com%2Fmetaverse-space%2Fblog-page%2F&amp;format=xml" />
<style type='text/css'> .ae_data .elementor-editor-element-setting {
            display:none !important;
            }
            </style>				<style type="text/css" id="cst_font_data">
					@font-face {font-family: "Good Times";font-display: auto;font-fallback: ;font-weight: 400;src: url(https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/good-times-rg.otf) format('OpenType');} @font-face {font-family: "Good Times";font-display: auto;font-fallback: ;font-weight: 700;src: url(https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/good-times-rg.otf) format('OpenType');} @font-face {font-family: "Purista";font-display: auto;font-fallback: ;font-weight: 500;src: url(https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/Purista-Medium.woff2) format('woff2'), url(https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/Purista-Medium.woff) format('woff'), url(https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/Purista-Medium.ttf) format('TrueType'), url(https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/Purista-Medium.eot) format('eot');} @font-face {font-family: "Purista";font-display: auto;font-fallback: ;font-weight: 300;src: url(https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/Purista-Light.woff2) format('woff2'), url(https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/Purista-Light.woff) format('woff'), url(https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/Purista-Light.ttf) format('TrueType'), url(https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/Purista-Light.eot) format('eot');} @font-face {font-family: "Purista";font-display: auto;font-fallback: ;font-weight: 600;src: url(https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/Purista-SemiBold.woff2) format('woff2'), url(https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/Purista-SemiBold.woff) format('woff'), url(https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/Purista-SemiBold.ttf) format('TrueType'), url(https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/Purista-SemiBold.eot) format('eot');}				</style>
						<style id="wp-custom-css">
			.elementor-element-5c90295d,
[data-carousel-3d] .slide p{
	display: none !important;
}

body{
	background: #000 !important;
}

.hfe-nav-menu__layout-horizontal .hfe-nav-menu .sub-arrow {
    margin-left: 10px;
    display: none;
}


/***
* class: .sticky-header
*/
header.sticky-header {
	--header-height: 100px;
	--shrink-header-to: 0.6;
	--transition: .45s cubic-bezier(.4, 0, .2, 1);
	background-color: rgba(244, 245, 248, 1);
	transition: background-color var(--transition),
				backdrop-filter var(--transition),
				box-shadow var(--transition);
}

/***
* Sticky header activated
*/
header.sticky-header.elementor-sticky--effects {
	background-color: rgba(244, 245, 248, .8);
	box-shadow: 0px 4px 33px 1px rgba(0, 0, 0, .07);
	-webkit-backdrop-filter: saturate(180%) blur(20px);
	backdrop-filter: saturate(180%) blur(20px);
}
header.sticky-header > .elementor-container {
	min-height: var(--header-height);
	transition: min-height var(--transition);
}
header.sticky-header.elementor-sticky--effects > .elementor-container {
	min-height: calc( var(--header-height) * var(--shrink-header-to) );
}

/***
* Shorter header on mobile (70px instead of 100px)
*/
@media only screen and (max-width: 767px) {
	header.sticky-header {
		--header-height: 70px;
	}
}

/***
* class: .logo
*/
header.sticky-header .logo img {
	transition: transform var(--transition);
}
header.sticky-header.elementor-sticky--effects .logo img {
	transform: scale(.8);
}


.image-container2 {
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-template-rows: repeat(4, 1fr);
  width: 100%;
  grid-gap: 0.5rem;
}
.image-container2 .image {
  position: relative;
  padding-bottom: 100%;
  /* width: 249px; */
}
.image-container2 .image img {
  height: 100%;
  width: 100%;
  object-fit: cover;
  left: 0;
  position: absolute;
  top: 0;
}
.image-container2 .image img:nth-of-type(1) {
/*   filter: grayscale(1) brightness(40%); */
}
.image-container2 .image img:nth-of-type(2) {
  clip-path: var(--clip-start);
  transition: clip-path 0.5s;
}
.image-container2 .image:hover {
  filter: grayscale(1) brightness(100%);
}


body{    
	background-image: url(https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/bg.png) !important;
    background-position: top center !important;
    background-repeat: no-repeat !important;
    background-size: contain !important;
	background-attachment: fixed !important
	}


[data-child-frame]{    border: 2px solid #4D14CD !important;
    padding: 10px;
    border-radius: 20px;
width:98% !important;
height: 100% !important;}

[data-carousel-3d] .slide{
    width: 400px !important;
	height: 400px !important;
	border-radius: 10px !important;


}


[data-carousel-3d]{

	height: 500px !important;
}


.swiper-slide .swiper-slide-inner {
	    background-color: transparent;
    background-image: radial-gradient(at center center, #8A04B15D 10%, #00000000 60%) !important;
}



.meta-partners .swiper-slide .swiper-slide-inner {
	    background-color: transparent;
    background-image: none !important;
}

[data-carousel-3d] [data-next-button]:before{
	content:url(https://ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/ic_arrow_right.png) !important;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    width: 65px!important;
    height: 65px !important;

	font-size: 35px !important;
	color: #fff !important;
	background-color: #510a70 !important;
	border-radius: 45px;
	text-align: center;
	
	margin-right: 0px !important;
}



[data-carousel-3d] [data-prev-button]:before{
	content:url(https://ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/ic_arrow_left.png) !important;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    width: 65px!important;
    height: 65px !important;

	font-size: 35px !important;
	color: #fff !important;
	background-color: #510a70 !important;
	border-radius: 45px;
	text-align: center;
	margin-left: 0px !important;
	
}


.w3-badge{
	height: 30px!IMPORTANT;
    width: 30px !important;
    padding: 0;
	
}

.w3-white, .w3-hover-white:hover {
    color: transparent!important;
    background-color: transparent!important;
}

.w3-border {
    border: 0px solid #ccc!important;
}

.w3-border {
    border: 0px solid #ccc!important;
}

.w3-badge:before{
	content:"\25C6";
	color: #808082;
	font-size: 30px !important;

	
	
}


.w3-badge:hover:before{
	content:"\25C6";
	color: #4d14cd;
	font-size: 30px !important;

	
	
}


.w3-badge1:before{
	content:"\25C6" !important;
	color: #4d14cd !important;
	font-size: 30px !important;
	
}


@import url('https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@400;700&amp;display=swap');

*{
	margin: 0;
	padding: 0;
	box-sizing: border-box;
	font-family: 'Noto Sans KR', sans-serif;
}

body{
	background: #ffda70;
}

.header{
	width: 1000px;
	padding: 20px;
	background: #7690da;
	margin: 25px auto;
	border-radius: 5px;
	text-align: center;
}

.header p{
	font-size: 45px;
	text-transform: uppercase;
	font-weight: 700;
	color: #fff;
}

.container .input{
	border: 0;
	outline: none;
	color: #8b7d77;
}

.search_wrap{
	width: 500px;
	margin: 38px auto;
}

.search_wrap .search_box{
	position: relative;
	width: 500px;
	height: 60px;
}

.search_wrap .search_box .input{
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	padding: 10px 20px;
	border-radius: 3px;
	font-size: 18px;
 max-width: 400px;
}

.search_wrap .search_box .btn{
	position: absolute;
	top: 0;
	right: 0;
	width: 60px;
	height: 80%;
	background: #4D14CD;
	z-index: 1;
	cursor: pointer;
 margin-right: 108px;
    margin-top: 5px;
    border: solid #453172 6px;
}

.search_wrap .search_box .btn:hover{
	background: #708bd2;	
}

.search_wrap .search_box .btn.btn_common .fas{
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%,-50%);
	color: #fff;
	font-size: 20px;
}



.search_wrap.search_wrap_3 .search_box .input{
	padding-right: 80px;
border-radius: 50px;
}


.search_wrap.search_wrap_3 .search_box .btn{
	right: 0px;
	border-radius: 50%;
}




/***
* Fan profile tabs header bg color and line
*/
.elementor-794 .elementor-element.elementor-element-78a3eed .elementor-tab-title.elementor-active, .elementor-794 .elementor-element.elementor-element-78a3eed .elementor-tab-title.elementor-active a {
    color: #FFFFFF;
    background-image: linear-gradient(#4D14CD, #510A70);
   
}

/*  Newsletter CSS*/

.field-btn-container {
    display: flex;
}

.field-btn-container .wpcf7-submit {
    border-radius: 0;
    background-color: #238AE0;
}

.field-btn-container .wpcf7-submit:hover {
    color: #238AE0;
    background-color: #fff;
}

/* blog page - tabs title*/

.elementor-1093 .elementor-element.elementor-element-b9abaac .elementor-tab-title.elementor-active, .elementor-1093 .elementor-element.elementor-element-b9abaac .elementor-tab-title.elementor-active a {
   
    background-image: linear-gradient(270deg, #238AE0 , #76BFF6, #238AE0 );
border-radius: 5px;
}
.elementor-1093 .elementor-element.elementor-element-b9abaac .elementor-tab-title {
border-radius: 5px;
	padding-right: 50px;
	padding-left: 50px;
	padding-top: 15px;
	padding-bottom: 15px;
    text-shadow: 0px 0px 0px rgb(0 0 0 / 30%);
    background-color: white;
    box-shadow: 0 0.5px 10px rgb(0 0 0 / 20%);
    margin: 10px;
margin-left: 60px
}

.elementor-1093 .elementor-element.elementor-element-e6b2f67 .elementor-button {

    background-image: linear-gradient(270deg, #76BFF6, #238AE0, #76BFF6 );
}

.elementor-1254 .elementor-element.elementor-element-f3e6d67 .elementor-button {
    background-image: linear-gradient(270deg, #76BFF6, #238AE0, #76BFF6 );
}

@font-face {
    font-family: 'Purista';
    src: url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-MediumItalic.eot');
    src: local('Purista Medium Italic'), local('Purista-MediumItalic'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-MediumItalic.eot?#iefix') format('embedded-opentype'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-MediumItalic.woff2') format('woff2'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-MediumItalic.woff') format('woff'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-MediumItalic.ttf') format('truetype');
    font-weight: 500;
    font-style: italic;
}

@font-face {
    font-family: 'Purista';
    src: url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-Medium.eot');
    src: local('Purista Medium'), local('Purista-Medium'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-Medium.eot?#iefix') format('embedded-opentype'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-Medium.woff2') format('woff2'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-Medium.woff') format('woff'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-Medium.ttf') format('truetype');
    font-weight: 500;
    font-style: normal;
}

@font-face {
    font-family: 'Purista';
    src: url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-SemiBoldItalic.eot');
    src: local('Purista SemiBold Italic'), local('Purista-SemiBoldItalic'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-SemiBoldItalic.eot?#iefix') format('embedded-opentype'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-SemiBoldItalic.woff2') format('woff2'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-SemiBoldItalic.woff') format('woff'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-SemiBoldItalic.ttf') format('truetype');
    font-weight: 600;
    font-style: italic;
}

@font-face {
    font-family: 'Purista';
    src: url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-LightItalic.eot');
    src: local('Purista Light Italic'), local('Purista-LightItalic'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-LightItalic.eot?#iefix') format('embedded-opentype'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-LightItalic.woff2') format('woff2'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-LightItalic.woff') format('woff'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-LightItalic.ttf') format('truetype');
    font-weight: 300;
    font-style: italic;
}

@font-face {
    font-family: 'Purista';
    src: url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-Light.eot');
    src: local('Purista Light'), local('Purista-Light'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-Light.eot?#iefix') format('embedded-opentype'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-Light.woff2') format('woff2'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-Light.woff') format('woff'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-Light.ttf') format('truetype');
    font-weight: 300;
    font-style: normal;
}

@font-face {
    font-family: 'Purista';
    src: url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-ThinItalic.eot');
    src: local('Purista Thin Italic'), local('Purista-ThinItalic'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-ThinItalic.eot?#iefix') format('embedded-opentype'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-ThinItalic.woff2') format('woff2'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-ThinItalic.woff') format('woff'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-ThinItalic.ttf') format('truetype');
    font-weight: 100;
    font-style: italic;
}

@font-face {
    font-family: 'Purista';
    src: url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-Bold.eot');
    src: local('Purista Bold'), local('Purista-Bold'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-Bold.eot?#iefix') format('embedded-opentype'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-Bold.woff2') format('woff2'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-Bold.woff') format('woff'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-Bold.ttf') format('truetype');
    font-weight: bold;
    font-style: normal;
}

@font-face {
    font-family: 'Purista';
    src: url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-SemiBold.eot');
    src: local('Purista SemiBold'), local('Purista-SemiBold'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-SemiBold.eot?#iefix') format('embedded-opentype'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-SemiBold.woff2') format('woff2'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-SemiBold.woff') format('woff'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-SemiBold.ttf') format('truetype');
    font-weight: 600;
    font-style: normal;
}

@font-face {
    font-family: 'Purista';
    src: url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-BoldItalic.eot');
    src: local('Purista Bold Italic'), local('Purista-BoldItalic'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-BoldItalic.eot?#iefix') format('embedded-opentype'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-BoldItalic.woff2') format('woff2'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-BoldItalic.woff') format('woff'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-BoldItalic.ttf') format('truetype');
    font-weight: bold;
    font-style: italic;
}

@font-face {
    font-family: 'Purista';
    src: url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-Thin.eot');
    src: local('Purista Thin'), local('Purista-Thin'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-Thin.eot?#iefix') format('embedded-opentype'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-Thin.woff2') format('woff2'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-Thin.woff') format('woff'),
        url('https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/Purista-Thin.ttf') format('truetype');
    font-weight: 100;
    font-style: normal;
}


		</style>
		</head>

<body itemtype='https://schema.org/WebPage' itemscope='itemscope' class="page-template-default page page-id-1093 page-child parent-pageid-1008 ehf-header ehf-footer ehf-template-astra ehf-stylesheet-astra group-blog ast-single-post ast-inherit-site-logo-transparent ast-hfb-header ast-desktop ast-page-builder-template ast-no-sidebar astra-3.9.1 elementor-default elementor-kit-7 elementor-page elementor-page-1093">

<a
	class="skip-link screen-reader-text"
	href="#content"
	role="link"
	title="Skip to content">
		Skip to content</a>

<div
class="hfeed site" id="page">
			@include('header')

		<div id="content" class="site-content">
		<div class="ast-container">
		

	<div id="primary" class="content-area primary">

		
					<main id="main" class="site-main">
				<article
class="post-1093 page type-page status-publish ast-article-single" id="post-1093" itemtype="https://schema.org/CreativeWork" itemscope="itemscope">
		<header class="entry-header ast-header-without-markup">
		
			</header><!-- .entry-header -->

	<div class="entry-content clear"
		itemprop="text"	>

		
				<div data-elementor-type="wp-page" data-elementor-id="1093" class="elementor elementor-1093">
									<section class="elementor-section elementor-top-section elementor-element elementor-element-e2dd5c5 elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id="e2dd5c5" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-b0a58e1" data-id="b0a58e1" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-2c4db0b elementor-widget elementor-widget-heading" data-id="2c4db0b" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<style>/*! elementor - v3.7.3 - 29-08-2022 */
.elementor-heading-title{padding:0;margin:0;line-height:1}.elementor-widget-heading .elementor-heading-title[class*=elementor-size-]>a{color:inherit;font-size:inherit;line-height:inherit}.elementor-widget-heading .elementor-heading-title.elementor-size-small{font-size:15px}.elementor-widget-heading .elementor-heading-title.elementor-size-medium{font-size:19px}.elementor-widget-heading .elementor-heading-title.elementor-size-large{font-size:29px}.elementor-widget-heading .elementor-heading-title.elementor-size-xl{font-size:39px}.elementor-widget-heading .elementor-heading-title.elementor-size-xxl{font-size:59px}</style><h2 class="elementor-heading-title elementor-size-default">Spooky Night Hallowen Event</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-6f9a6c1 elementor-widget elementor-widget-text-editor" data-id="6f9a6c1" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
			<style>/*! elementor - v3.7.3 - 29-08-2022 */
.elementor-widget-text-editor.elementor-drop-cap-view-stacked .elementor-drop-cap{background-color:#818a91;color:#fff}.elementor-widget-text-editor.elementor-drop-cap-view-framed .elementor-drop-cap{color:#818a91;border:3px solid;background-color:transparent}.elementor-widget-text-editor:not(.elementor-drop-cap-view-default) .elementor-drop-cap{margin-top:8px}.elementor-widget-text-editor:not(.elementor-drop-cap-view-default) .elementor-drop-cap-letter{width:1em;height:1em}.elementor-widget-text-editor .elementor-drop-cap{float:left;text-align:center;line-height:1;font-size:50px}.elementor-widget-text-editor .elementor-drop-cap-letter{display:inline-block}</style>				<p>Face the Meta-spooky Night!</p>						</div>
				</div>
				<div class="elementor-element elementor-element-e6b2f67 button-3 elementor-widget elementor-widget-button" data-id="e6b2f67" data-element_type="widget" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="#" class="elementor-button-link elementor-button elementor-size-sm" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">Read More</span>
		</span>
					</a>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-b43cd73 elementor-widget elementor-widget-html" data-id="b43cd73" data-element_type="widget" data-widget_type="html.default">
				<div class="elementor-widget-container">
			<style>
    .dot {
  cursor: pointer;
  height: 5px;
  width: 50px;
  margin: 0 2px;
  background-color: white;
 
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active, .dot:hover {
  background-color: #2388DC;
}
</style>

<div style="text-align:center">
  <span class="active dot" onclick="currentSlide(1)"></span> 
  <span class="dot" onclick="currentSlide(2)"></span> 
  <span class="dot" onclick="currentSlide(3)"></span> 
  <span class="dot" onclick="currentSlide(4)"></span> 
</div>		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-1cf688c elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id="1cf688c" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-6884c6f" data-id="6884c6f" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-b9abaac fan-profile-tab-list elementor-tabs-alignment-center elementor-tabs-view-horizontal elementor-widget elementor-widget-tabs" data-id="b9abaac" data-element_type="widget" data-widget_type="tabs.default">
				<div class="elementor-widget-container">
			<style>/*! elementor - v3.7.3 - 29-08-2022 */
.elementor-widget-tabs.elementor-tabs-view-vertical .elementor-tabs-wrapper{width:25%;-ms-flex-negative:0;flex-shrink:0}.elementor-widget-tabs.elementor-tabs-view-vertical .elementor-tab-desktop-title.elementor-active{border-right-style:none}.elementor-widget-tabs.elementor-tabs-view-vertical .elementor-tab-desktop-title.elementor-active:after,.elementor-widget-tabs.elementor-tabs-view-vertical .elementor-tab-desktop-title.elementor-active:before{height:999em;width:0;right:0;border-right-style:solid}.elementor-widget-tabs.elementor-tabs-view-vertical .elementor-tab-desktop-title.elementor-active:before{top:0;-webkit-transform:translateY(-100%);-ms-transform:translateY(-100%);transform:translateY(-100%)}.elementor-widget-tabs.elementor-tabs-view-vertical .elementor-tab-desktop-title.elementor-active:after{top:100%}.elementor-widget-tabs.elementor-tabs-view-horizontal .elementor-tab-desktop-title{display:table-cell}.elementor-widget-tabs.elementor-tabs-view-horizontal .elementor-tab-desktop-title.elementor-active{border-bottom-style:none}.elementor-widget-tabs.elementor-tabs-view-horizontal .elementor-tab-desktop-title.elementor-active:after,.elementor-widget-tabs.elementor-tabs-view-horizontal .elementor-tab-desktop-title.elementor-active:before{bottom:0;height:0;width:999em;border-bottom-style:solid}.elementor-widget-tabs.elementor-tabs-view-horizontal .elementor-tab-desktop-title.elementor-active:before{right:100%}.elementor-widget-tabs.elementor-tabs-view-horizontal .elementor-tab-desktop-title.elementor-active:after{left:100%}.elementor-widget-tabs .elementor-tab-content,.elementor-widget-tabs .elementor-tab-title,.elementor-widget-tabs .elementor-tab-title:after,.elementor-widget-tabs .elementor-tab-title:before,.elementor-widget-tabs .elementor-tabs-content-wrapper{border:1px #d4d4d4}.elementor-widget-tabs .elementor-tabs{text-align:left}.elementor-widget-tabs .elementor-tabs-wrapper{overflow:hidden}.elementor-widget-tabs .elementor-tab-title{cursor:pointer;outline:var(--focus-outline,none)}.elementor-widget-tabs .elementor-tab-desktop-title{position:relative;padding:20px 25px;font-weight:700;line-height:1;border:solid transparent}.elementor-widget-tabs .elementor-tab-desktop-title.elementor-active{border-color:#d4d4d4}.elementor-widget-tabs .elementor-tab-desktop-title.elementor-active:after,.elementor-widget-tabs .elementor-tab-desktop-title.elementor-active:before{display:block;content:"";position:absolute}.elementor-widget-tabs .elementor-tab-desktop-title:focus-visible{border:1px solid #000}.elementor-widget-tabs .elementor-tab-mobile-title{padding:10px;cursor:pointer}.elementor-widget-tabs .elementor-tab-content{padding:20px;display:none}@media (max-width:767px){.elementor-tabs .elementor-tab-content,.elementor-tabs .elementor-tab-title{border-style:solid solid none}.elementor-tabs .elementor-tabs-wrapper{display:none}.elementor-tabs .elementor-tabs-content-wrapper{border-bottom-style:solid}.elementor-tabs .elementor-tab-content{padding:10px}}@media (min-width:768px){.elementor-widget-tabs.elementor-tabs-view-vertical .elementor-tabs{display:-webkit-box;display:-ms-flexbox;display:flex}.elementor-widget-tabs.elementor-tabs-view-vertical .elementor-tabs-wrapper{-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column}.elementor-widget-tabs.elementor-tabs-view-vertical .elementor-tabs-content-wrapper{-webkit-box-flex:1;-ms-flex-positive:1;flex-grow:1;border-style:solid solid solid none}.elementor-widget-tabs.elementor-tabs-view-horizontal .elementor-tab-content{border-style:none solid solid}.elementor-widget-tabs.elementor-tabs-alignment-center .elementor-tabs-wrapper,.elementor-widget-tabs.elementor-tabs-alignment-end .elementor-tabs-wrapper,.elementor-widget-tabs.elementor-tabs-alignment-stretch .elementor-tabs-wrapper{display:-webkit-box;display:-ms-flexbox;display:flex}.elementor-widget-tabs.elementor-tabs-alignment-center .elementor-tabs-wrapper{-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center}.elementor-widget-tabs.elementor-tabs-alignment-end .elementor-tabs-wrapper{-webkit-box-pack:end;-ms-flex-pack:end;justify-content:flex-end}.elementor-widget-tabs.elementor-tabs-alignment-stretch.elementor-tabs-view-horizontal .elementor-tab-title{width:100%}.elementor-widget-tabs.elementor-tabs-alignment-stretch.elementor-tabs-view-vertical .elementor-tab-title{height:100%}.elementor-tabs .elementor-tab-mobile-title{display:none}}</style>		<div class="elementor-tabs">
			<div class="elementor-tabs-wrapper" role="tablist" >
									<div id="elementor-tab-title-1941" class="elementor-tab-title elementor-tab-desktop-title" aria-selected="true" data-tab="1" role="tab" tabindex="0" aria-controls="elementor-tab-content-1941" aria-expanded="false">All Events</div>
									<div id="elementor-tab-title-1942" class="elementor-tab-title elementor-tab-desktop-title" aria-selected="false" data-tab="2" role="tab" tabindex="-1" aria-controls="elementor-tab-content-1942" aria-expanded="false">Upcoming Events</div>
									<div id="elementor-tab-title-1943" class="elementor-tab-title elementor-tab-desktop-title" aria-selected="false" data-tab="3" role="tab" tabindex="-1" aria-controls="elementor-tab-content-1943" aria-expanded="false">Recent Events</div>
									<div id="elementor-tab-title-1944" class="elementor-tab-title elementor-tab-desktop-title" aria-selected="false" data-tab="4" role="tab" tabindex="-1" aria-controls="elementor-tab-content-1944" aria-expanded="false">Past Events</div>
							</div>
			<div class="elementor-tabs-content-wrapper" role="tablist" aria-orientation="vertical">
									<div class="elementor-tab-title elementor-tab-mobile-title" aria-selected="true" data-tab="1" role="tab" tabindex="0" aria-controls="elementor-tab-content-1941" aria-expanded="false">All Events</div>
					<div id="elementor-tab-content-1941" class="elementor-tab-content elementor-clearfix" data-tab="1" role="tabpanel" aria-labelledby="elementor-tab-title-1941" tabindex="0" hidden="false"><p>		<div data-elementor-type="wp-post" data-elementor-id="1254" class="elementor elementor-1254">
									<section class="elementor-section elementor-top-section elementor-element elementor-element-17984f7 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="17984f7" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-37e798b" data-id="37e798b" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-5e24236 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="5e24236" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-5051826" data-id="5051826" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-8fc9ce2 elementor-widget elementor-widget-image" data-id="8fc9ce2" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="372" height="227" src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/img_events_details.png?time=1664353118" class="attachment-large size-large" alt="" loading="lazy" srcset="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/img_events_details.png 372w, https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/img_events_details-300x183.png 300w" sizes="(max-width: 372px) 100vw, 372px" />															</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-ed3b14b" data-id="ed3b14b" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-155b871 elementor-widget elementor-widget-heading" data-id="155b871" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">SPOOKY NIGHT HALLOWEEN SPECIAL</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-66ba70c elementor-widget elementor-widget-text-editor" data-id="66ba70c" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>When Halloween rolls around, black cat face painting helps capture the magic and mystery of the beloved frightful night. If it&#8217;s not out by Halloween, it soon will be. In the wake of the Mad Moon&#8217;s destruction, willful fragments of Radiant Ore and Direstone crashed upon our world.</p>						</div>
				</div>
				<div class="elementor-element elementor-element-e103dc0 elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="e103dc0" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>October 29, 2022</p>						</div>
				</div>
				<div class="elementor-element elementor-element-c32fbff elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="c32fbff" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>5min Read</p>						</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-bef1baf elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="bef1baf" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-d776e03" data-id="d776e03" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-4071c65 elementor-widget elementor-widget-image" data-id="4071c65" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="372" height="227" src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/newitem.png?time=1664353118" class="attachment-large size-large" alt="" loading="lazy" srcset="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/newitem.png 372w, https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/newitem-300x183.png 300w" sizes="(max-width: 372px) 100vw, 372px" />															</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-d35c9c8" data-id="d35c9c8" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-4e7b47b elementor-widget elementor-widget-heading" data-id="4e7b47b" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">NEW ITEM CHESTS</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-abd8346 elementor-widget elementor-widget-text-editor" data-id="abd8346" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>Winter is just around the corner and now that the New Map Update is live, we&#8217;re setting our sights on new item chests to be released this October! Tune in and buy the item in shop.</p>						</div>
				</div>
				<div class="elementor-element elementor-element-225e998 elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="225e998" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>October 29, 2022</p>						</div>
				</div>
				<div class="elementor-element elementor-element-4876dcb elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="4876dcb" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>4min Read</p>						</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-cfc8085 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="cfc8085" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-f995f9d" data-id="f995f9d" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-5288cac elementor-widget elementor-widget-image" data-id="5288cac" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="372" height="227" src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/xmas.png?time=1664353118" class="attachment-large size-large" alt="" loading="lazy" srcset="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/xmas.png 372w, https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/xmas-300x183.png 300w" sizes="(max-width: 372px) 100vw, 372px" />															</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-eb6fa88" data-id="eb6fa88" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-590a85c elementor-widget elementor-widget-heading" data-id="590a85c" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">META VERSE CHRISTMAS EVENT UPDATE</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-c7f0c8e elementor-widget elementor-widget-text-editor" data-id="c7f0c8e" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>Christmas is Here! Metaverse Christmas party event! Join us to celebrate the most wonderful time of the year with the most wonderful people. A drawing of lots will also be held on the 15th of December.</p>						</div>
				</div>
				<div class="elementor-element elementor-element-0571453 elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="0571453" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>October 9, 2022</p>						</div>
				</div>
				<div class="elementor-element elementor-element-bd53f1d elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="bd53f1d" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>2min Read</p>						</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-ac31c25 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="ac31c25" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-9671e37" data-id="9671e37" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-1afcfa4 elementor-widget elementor-widget-image" data-id="1afcfa4" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="372" height="227" src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/newgame.png?time=1664353118" class="attachment-large size-large" alt="" loading="lazy" srcset="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/newgame.png 372w, https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/newgame-300x183.png 300w" sizes="(max-width: 372px) 100vw, 372px" />															</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-d64df4b" data-id="d64df4b" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-238fddd elementor-widget elementor-widget-heading" data-id="238fddd" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">NEW GAME EVENT MAP</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-ee27963 elementor-widget elementor-widget-text-editor" data-id="ee27963" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>As winter winds arrive to help break summer&#8217;s final grip, it&#8217;s time to welcome a new Meta verse map update. Download and earn a reward upon first login on the game. Make sure to enter your predictions in the Battle Pass today.</p>						</div>
				</div>
				<div class="elementor-element elementor-element-91df2eb elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="91df2eb" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>July 31, 2022</p>						</div>
				</div>
				<div class="elementor-element elementor-element-71f5a68 elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="71f5a68" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>2min Read</p>						</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<div class="elementor-element elementor-element-f3e6d67 elementor-align-center elementor-widget__width-inherit button-3 elementor-widget elementor-widget-button" data-id="f3e6d67" data-element_type="widget" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="#" class="elementor-button-link elementor-button elementor-size-sm" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">Load More</span>
		</span>
					</a>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
							</div>
		</p></div>
									<div class="elementor-tab-title elementor-tab-mobile-title" aria-selected="false" data-tab="2" role="tab" tabindex="-1" aria-controls="elementor-tab-content-1942" aria-expanded="false">Upcoming Events</div>
					<div id="elementor-tab-content-1942" class="elementor-tab-content elementor-clearfix" data-tab="2" role="tabpanel" aria-labelledby="elementor-tab-title-1942" tabindex="0" hidden="hidden"><p>		<div data-elementor-type="wp-post" data-elementor-id="1254" class="elementor elementor-1254">
									<section class="elementor-section elementor-top-section elementor-element elementor-element-17984f7 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="17984f7" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-37e798b" data-id="37e798b" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-5e24236 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="5e24236" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-5051826" data-id="5051826" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-8fc9ce2 elementor-widget elementor-widget-image" data-id="8fc9ce2" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="372" height="227" src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/img_events_details.png?time=1664353118" class="attachment-large size-large" alt="" loading="lazy" srcset="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/img_events_details.png 372w, https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/img_events_details-300x183.png 300w" sizes="(max-width: 372px) 100vw, 372px" />															</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-ed3b14b" data-id="ed3b14b" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-155b871 elementor-widget elementor-widget-heading" data-id="155b871" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">SPOOKY NIGHT HALLOWEEN SPECIAL</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-66ba70c elementor-widget elementor-widget-text-editor" data-id="66ba70c" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>When Halloween rolls around, black cat face painting helps capture the magic and mystery of the beloved frightful night. If it&#8217;s not out by Halloween, it soon will be. In the wake of the Mad Moon&#8217;s destruction, willful fragments of Radiant Ore and Direstone crashed upon our world.</p>						</div>
				</div>
				<div class="elementor-element elementor-element-e103dc0 elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="e103dc0" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>October 29, 2022</p>						</div>
				</div>
				<div class="elementor-element elementor-element-c32fbff elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="c32fbff" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>5min Read</p>						</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-bef1baf elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="bef1baf" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-d776e03" data-id="d776e03" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-4071c65 elementor-widget elementor-widget-image" data-id="4071c65" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="372" height="227" src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/newitem.png?time=1664353118" class="attachment-large size-large" alt="" loading="lazy" srcset="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/newitem.png 372w, https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/newitem-300x183.png 300w" sizes="(max-width: 372px) 100vw, 372px" />															</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-d35c9c8" data-id="d35c9c8" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-4e7b47b elementor-widget elementor-widget-heading" data-id="4e7b47b" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">NEW ITEM CHESTS</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-abd8346 elementor-widget elementor-widget-text-editor" data-id="abd8346" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>Winter is just around the corner and now that the New Map Update is live, we&#8217;re setting our sights on new item chests to be released this October! Tune in and buy the item in shop.</p>						</div>
				</div>
				<div class="elementor-element elementor-element-225e998 elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="225e998" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>October 29, 2022</p>						</div>
				</div>
				<div class="elementor-element elementor-element-4876dcb elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="4876dcb" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>4min Read</p>						</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-cfc8085 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="cfc8085" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-f995f9d" data-id="f995f9d" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-5288cac elementor-widget elementor-widget-image" data-id="5288cac" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="372" height="227" src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/xmas.png?time=1664353118" class="attachment-large size-large" alt="" loading="lazy" srcset="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/xmas.png 372w, https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/xmas-300x183.png 300w" sizes="(max-width: 372px) 100vw, 372px" />															</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-eb6fa88" data-id="eb6fa88" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-590a85c elementor-widget elementor-widget-heading" data-id="590a85c" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">META VERSE CHRISTMAS EVENT UPDATE</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-c7f0c8e elementor-widget elementor-widget-text-editor" data-id="c7f0c8e" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>Christmas is Here! Metaverse Christmas party event! Join us to celebrate the most wonderful time of the year with the most wonderful people. A drawing of lots will also be held on the 15th of December.</p>						</div>
				</div>
				<div class="elementor-element elementor-element-0571453 elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="0571453" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>October 9, 2022</p>						</div>
				</div>
				<div class="elementor-element elementor-element-bd53f1d elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="bd53f1d" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>2min Read</p>						</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-ac31c25 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="ac31c25" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-9671e37" data-id="9671e37" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-1afcfa4 elementor-widget elementor-widget-image" data-id="1afcfa4" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="372" height="227" src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/newgame.png?time=1664353118" class="attachment-large size-large" alt="" loading="lazy" srcset="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/newgame.png 372w, https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/newgame-300x183.png 300w" sizes="(max-width: 372px) 100vw, 372px" />															</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-d64df4b" data-id="d64df4b" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-238fddd elementor-widget elementor-widget-heading" data-id="238fddd" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">NEW GAME EVENT MAP</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-ee27963 elementor-widget elementor-widget-text-editor" data-id="ee27963" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>As winter winds arrive to help break summer&#8217;s final grip, it&#8217;s time to welcome a new Meta verse map update. Download and earn a reward upon first login on the game. Make sure to enter your predictions in the Battle Pass today.</p>						</div>
				</div>
				<div class="elementor-element elementor-element-91df2eb elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="91df2eb" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>July 31, 2022</p>						</div>
				</div>
				<div class="elementor-element elementor-element-71f5a68 elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="71f5a68" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>2min Read</p>						</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<div class="elementor-element elementor-element-f3e6d67 elementor-align-center elementor-widget__width-inherit button-3 elementor-widget elementor-widget-button" data-id="f3e6d67" data-element_type="widget" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="#" class="elementor-button-link elementor-button elementor-size-sm" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">Load More</span>
		</span>
					</a>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
							</div>
		</p></div>
									<div class="elementor-tab-title elementor-tab-mobile-title" aria-selected="false" data-tab="3" role="tab" tabindex="-1" aria-controls="elementor-tab-content-1943" aria-expanded="false">Recent Events</div>
					<div id="elementor-tab-content-1943" class="elementor-tab-content elementor-clearfix" data-tab="3" role="tabpanel" aria-labelledby="elementor-tab-title-1943" tabindex="0" hidden="hidden"><p>		<div data-elementor-type="wp-post" data-elementor-id="1254" class="elementor elementor-1254">
									<section class="elementor-section elementor-top-section elementor-element elementor-element-17984f7 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="17984f7" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-37e798b" data-id="37e798b" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-5e24236 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="5e24236" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-5051826" data-id="5051826" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-8fc9ce2 elementor-widget elementor-widget-image" data-id="8fc9ce2" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="372" height="227" src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/img_events_details.png?time=1664353118" class="attachment-large size-large" alt="" loading="lazy" srcset="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/img_events_details.png 372w, https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/img_events_details-300x183.png 300w" sizes="(max-width: 372px) 100vw, 372px" />															</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-ed3b14b" data-id="ed3b14b" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-155b871 elementor-widget elementor-widget-heading" data-id="155b871" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">SPOOKY NIGHT HALLOWEEN SPECIAL</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-66ba70c elementor-widget elementor-widget-text-editor" data-id="66ba70c" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>When Halloween rolls around, black cat face painting helps capture the magic and mystery of the beloved frightful night. If it&#8217;s not out by Halloween, it soon will be. In the wake of the Mad Moon&#8217;s destruction, willful fragments of Radiant Ore and Direstone crashed upon our world.</p>						</div>
				</div>
				<div class="elementor-element elementor-element-e103dc0 elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="e103dc0" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>October 29, 2022</p>						</div>
				</div>
				<div class="elementor-element elementor-element-c32fbff elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="c32fbff" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>5min Read</p>						</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-bef1baf elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="bef1baf" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-d776e03" data-id="d776e03" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-4071c65 elementor-widget elementor-widget-image" data-id="4071c65" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="372" height="227" src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/newitem.png?time=1664353118" class="attachment-large size-large" alt="" loading="lazy" srcset="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/newitem.png 372w, https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/newitem-300x183.png 300w" sizes="(max-width: 372px) 100vw, 372px" />															</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-d35c9c8" data-id="d35c9c8" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-4e7b47b elementor-widget elementor-widget-heading" data-id="4e7b47b" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">NEW ITEM CHESTS</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-abd8346 elementor-widget elementor-widget-text-editor" data-id="abd8346" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>Winter is just around the corner and now that the New Map Update is live, we&#8217;re setting our sights on new item chests to be released this October! Tune in and buy the item in shop.</p>						</div>
				</div>
				<div class="elementor-element elementor-element-225e998 elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="225e998" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>October 29, 2022</p>						</div>
				</div>
				<div class="elementor-element elementor-element-4876dcb elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="4876dcb" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>4min Read</p>						</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-cfc8085 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="cfc8085" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-f995f9d" data-id="f995f9d" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-5288cac elementor-widget elementor-widget-image" data-id="5288cac" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="372" height="227" src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/xmas.png?time=1664353118" class="attachment-large size-large" alt="" loading="lazy" srcset="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/xmas.png 372w, https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/xmas-300x183.png 300w" sizes="(max-width: 372px) 100vw, 372px" />															</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-eb6fa88" data-id="eb6fa88" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-590a85c elementor-widget elementor-widget-heading" data-id="590a85c" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">META VERSE CHRISTMAS EVENT UPDATE</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-c7f0c8e elementor-widget elementor-widget-text-editor" data-id="c7f0c8e" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>Christmas is Here! Metaverse Christmas party event! Join us to celebrate the most wonderful time of the year with the most wonderful people. A drawing of lots will also be held on the 15th of December.</p>						</div>
				</div>
				<div class="elementor-element elementor-element-0571453 elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="0571453" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>October 9, 2022</p>						</div>
				</div>
				<div class="elementor-element elementor-element-bd53f1d elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="bd53f1d" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>2min Read</p>						</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-ac31c25 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="ac31c25" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-9671e37" data-id="9671e37" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-1afcfa4 elementor-widget elementor-widget-image" data-id="1afcfa4" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="372" height="227" src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/newgame.png?time=1664353118" class="attachment-large size-large" alt="" loading="lazy" srcset="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/newgame.png 372w, https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/newgame-300x183.png 300w" sizes="(max-width: 372px) 100vw, 372px" />															</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-d64df4b" data-id="d64df4b" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-238fddd elementor-widget elementor-widget-heading" data-id="238fddd" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">NEW GAME EVENT MAP</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-ee27963 elementor-widget elementor-widget-text-editor" data-id="ee27963" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>As winter winds arrive to help break summer&#8217;s final grip, it&#8217;s time to welcome a new Meta verse map update. Download and earn a reward upon first login on the game. Make sure to enter your predictions in the Battle Pass today.</p>						</div>
				</div>
				<div class="elementor-element elementor-element-91df2eb elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="91df2eb" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>July 31, 2022</p>						</div>
				</div>
				<div class="elementor-element elementor-element-71f5a68 elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="71f5a68" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>2min Read</p>						</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<div class="elementor-element elementor-element-f3e6d67 elementor-align-center elementor-widget__width-inherit button-3 elementor-widget elementor-widget-button" data-id="f3e6d67" data-element_type="widget" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="#" class="elementor-button-link elementor-button elementor-size-sm" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">Load More</span>
		</span>
					</a>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
							</div>
		</p></div>
									<div class="elementor-tab-title elementor-tab-mobile-title" aria-selected="false" data-tab="4" role="tab" tabindex="-1" aria-controls="elementor-tab-content-1944" aria-expanded="false">Past Events</div>
					<div id="elementor-tab-content-1944" class="elementor-tab-content elementor-clearfix" data-tab="4" role="tabpanel" aria-labelledby="elementor-tab-title-1944" tabindex="0" hidden="hidden"><p>		<div data-elementor-type="wp-post" data-elementor-id="1254" class="elementor elementor-1254">
									<section class="elementor-section elementor-top-section elementor-element elementor-element-17984f7 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="17984f7" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-37e798b" data-id="37e798b" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-5e24236 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="5e24236" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-5051826" data-id="5051826" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-8fc9ce2 elementor-widget elementor-widget-image" data-id="8fc9ce2" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="372" height="227" src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/img_events_details.png?time=1664353118" class="attachment-large size-large" alt="" loading="lazy" srcset="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/img_events_details.png 372w, https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/img_events_details-300x183.png 300w" sizes="(max-width: 372px) 100vw, 372px" />															</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-ed3b14b" data-id="ed3b14b" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-155b871 elementor-widget elementor-widget-heading" data-id="155b871" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">SPOOKY NIGHT HALLOWEEN SPECIAL</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-66ba70c elementor-widget elementor-widget-text-editor" data-id="66ba70c" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>When Halloween rolls around, black cat face painting helps capture the magic and mystery of the beloved frightful night. If it&#8217;s not out by Halloween, it soon will be. In the wake of the Mad Moon&#8217;s destruction, willful fragments of Radiant Ore and Direstone crashed upon our world.</p>						</div>
				</div>
				<div class="elementor-element elementor-element-e103dc0 elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="e103dc0" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>October 29, 2022</p>						</div>
				</div>
				<div class="elementor-element elementor-element-c32fbff elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="c32fbff" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>5min Read</p>						</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-bef1baf elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="bef1baf" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-d776e03" data-id="d776e03" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-4071c65 elementor-widget elementor-widget-image" data-id="4071c65" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="372" height="227" src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/newitem.png?time=1664353118" class="attachment-large size-large" alt="" loading="lazy" srcset="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/newitem.png 372w, https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/newitem-300x183.png 300w" sizes="(max-width: 372px) 100vw, 372px" />															</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-d35c9c8" data-id="d35c9c8" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-4e7b47b elementor-widget elementor-widget-heading" data-id="4e7b47b" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">NEW ITEM CHESTS</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-abd8346 elementor-widget elementor-widget-text-editor" data-id="abd8346" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>Winter is just around the corner and now that the New Map Update is live, we&#8217;re setting our sights on new item chests to be released this October! Tune in and buy the item in shop.</p>						</div>
				</div>
				<div class="elementor-element elementor-element-225e998 elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="225e998" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>October 29, 2022</p>						</div>
				</div>
				<div class="elementor-element elementor-element-4876dcb elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="4876dcb" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>4min Read</p>						</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-cfc8085 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="cfc8085" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-f995f9d" data-id="f995f9d" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-5288cac elementor-widget elementor-widget-image" data-id="5288cac" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="372" height="227" src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/xmas.png?time=1664353118" class="attachment-large size-large" alt="" loading="lazy" srcset="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/xmas.png 372w, https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/xmas-300x183.png 300w" sizes="(max-width: 372px) 100vw, 372px" />															</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-eb6fa88" data-id="eb6fa88" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-590a85c elementor-widget elementor-widget-heading" data-id="590a85c" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">META VERSE CHRISTMAS EVENT UPDATE</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-c7f0c8e elementor-widget elementor-widget-text-editor" data-id="c7f0c8e" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>Christmas is Here! Metaverse Christmas party event! Join us to celebrate the most wonderful time of the year with the most wonderful people. A drawing of lots will also be held on the 15th of December.</p>						</div>
				</div>
				<div class="elementor-element elementor-element-0571453 elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="0571453" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>October 9, 2022</p>						</div>
				</div>
				<div class="elementor-element elementor-element-bd53f1d elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="bd53f1d" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>2min Read</p>						</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-ac31c25 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="ac31c25" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-9671e37" data-id="9671e37" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-1afcfa4 elementor-widget elementor-widget-image" data-id="1afcfa4" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="372" height="227" src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/newgame.png?time=1664353118" class="attachment-large size-large" alt="" loading="lazy" srcset="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/newgame.png 372w, https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/newgame-300x183.png 300w" sizes="(max-width: 372px) 100vw, 372px" />															</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-d64df4b" data-id="d64df4b" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-238fddd elementor-widget elementor-widget-heading" data-id="238fddd" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">NEW GAME EVENT MAP</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-ee27963 elementor-widget elementor-widget-text-editor" data-id="ee27963" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>As winter winds arrive to help break summer&#8217;s final grip, it&#8217;s time to welcome a new Meta verse map update. Download and earn a reward upon first login on the game. Make sure to enter your predictions in the Battle Pass today.</p>						</div>
				</div>
				<div class="elementor-element elementor-element-91df2eb elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="91df2eb" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>July 31, 2022</p>						</div>
				</div>
				<div class="elementor-element elementor-element-71f5a68 elementor-widget__width-auto elementor-widget elementor-widget-text-editor" data-id="71f5a68" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>2min Read</p>						</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<div class="elementor-element elementor-element-f3e6d67 elementor-align-center elementor-widget__width-inherit button-3 elementor-widget elementor-widget-button" data-id="f3e6d67" data-element_type="widget" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="#" class="elementor-button-link elementor-button elementor-size-sm" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">Load More</span>
		</span>
					</a>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
							</div>
		</p></div>
							</div>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
							</div>
		
		
		
	</div><!-- .entry-content .clear -->

	
	
</article><!-- #post-## -->

			</main><!-- #main -->
			
		
	</div><!-- #primary -->


	</div> <!-- ast-container -->
	</div><!-- #content -->
		<footer itemtype="https://schema.org/WPFooter" itemscope="itemscope" id="colophon" role="contentinfo">
			<div class='footer-width-fixer'>		<div data-elementor-type="wp-post" data-elementor-id="1063" class="elementor elementor-1063">
									<section class="elementor-section elementor-top-section elementor-element elementor-element-14065d8 elementor-section-content-middle elementor-hidden-tablet elementor-hidden-mobile elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="14065d8" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-2e8a3a5" data-id="2e8a3a5" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-48a6166 hfe-nav-menu__breakpoint-mobile hfe-nav-menu__align-left hfe-submenu-icon-arrow hfe-link-redirect-child elementor-widget elementor-widget-navigation-menu" data-id="48a6166" data-element_type="widget" data-settings="{&quot;padding_vertical_menu_item&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;padding_horizontal_menu_item&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:15,&quot;sizes&quot;:[]},&quot;padding_horizontal_menu_item_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;padding_horizontal_menu_item_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;padding_vertical_menu_item_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;padding_vertical_menu_item_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;menu_space_between&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;menu_space_between_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;menu_space_between_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;dropdown_border_radius&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;top&quot;:&quot;&quot;,&quot;right&quot;:&quot;&quot;,&quot;bottom&quot;:&quot;&quot;,&quot;left&quot;:&quot;&quot;,&quot;isLinked&quot;:true},&quot;dropdown_border_radius_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;top&quot;:&quot;&quot;,&quot;right&quot;:&quot;&quot;,&quot;bottom&quot;:&quot;&quot;,&quot;left&quot;:&quot;&quot;,&quot;isLinked&quot;:true},&quot;dropdown_border_radius_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;top&quot;:&quot;&quot;,&quot;right&quot;:&quot;&quot;,&quot;bottom&quot;:&quot;&quot;,&quot;left&quot;:&quot;&quot;,&quot;isLinked&quot;:true},&quot;padding_horizontal_dropdown_item&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;padding_horizontal_dropdown_item_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;padding_horizontal_dropdown_item_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;padding_vertical_dropdown_item&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:15,&quot;sizes&quot;:[]},&quot;padding_vertical_dropdown_item_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;padding_vertical_dropdown_item_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;distance_from_menu&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;distance_from_menu_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;distance_from_menu_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;toggle_size&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;toggle_size_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;toggle_size_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;toggle_border_width&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;toggle_border_width_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;toggle_border_width_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;toggle_border_radius&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;toggle_border_radius_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;toggle_border_radius_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]}}" data-widget_type="navigation-menu.default">
				<div class="elementor-widget-container">
						<div class="hfe-nav-menu hfe-layout-vertical hfe-nav-menu-layout vertical" data-layout="vertical">
				<div class="hfe-nav-menu__toggle elementor-clickable">
					<div class="hfe-nav-menu-icon">
						<i aria-hidden="true" tabindex="0" class="fas fa-align-justify"></i>					</div>
				</div>
				<nav class="hfe-nav-menu__layout-vertical hfe-nav-menu__submenu-arrow" data-toggle-icon="&lt;i aria-hidden=&quot;true&quot; tabindex=&quot;0&quot; class=&quot;fas fa-align-justify&quot;&gt;&lt;/i&gt;" data-close-icon="&lt;i aria-hidden=&quot;true&quot; tabindex=&quot;0&quot; class=&quot;far fa-window-close&quot;&gt;&lt;/i&gt;" data-full-width="yes"><ul id="menu-1-48a6166" class="hfe-nav-menu"><li id="menu-item-1012" class="menu-item menu-item-type-custom menu-item-object-custom parent hfe-creative-menu"><a href="https://ihk.8a1.myftpupload.com/metaverse-space/" class = "hfe-menu-item">Home</a></li>
<li id="menu-item-1013" class="menu-item menu-item-type-custom menu-item-object-custom parent hfe-creative-menu"><a href="#about" class = "hfe-menu-item">About</a></li>
<li id="menu-item-1014" class="menu-item menu-item-type-custom menu-item-object-custom parent hfe-creative-menu"><a href="#contactus" class = "hfe-menu-item">Contact Us</a></li>
<li id="menu-item-1015" class="menu-item menu-item-type-custom menu-item-object-custom parent hfe-creative-menu"><a href="https://ihk.8a1.myftpupload.com/blog-page/" class = "hfe-menu-item">Events</a></li>
</ul></nav>              
			</div>
					</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-e75c300" data-id="e75c300" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-fcc08af elementor-widget elementor-widget-image" data-id="fcc08af" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
																<a href="https://ihk.8a1.myftpupload.com/metaverse-space/">
							<img width="287" height="49" src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/logo.png?time=1664353118" class="attachment-medium size-medium" alt="" loading="lazy" />								</a>
															</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-60f659a" data-id="60f659a" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-aed3349 elementor-widget elementor-widget-heading" data-id="aed3349" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h3 class="elementor-heading-title elementor-size-default">Newsletter</h3>		</div>
				</div>
				<div class="elementor-element elementor-element-e950f0c elementor-widget elementor-widget-text-editor" data-id="e950f0c" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							Sign up for our newsletter to get the latest updates &amp; special offers						</div>
				</div>
				<div class="elementor-element elementor-element-600bb44 elementor-widget elementor-widget-premium-contact-form" data-id="600bb44" data-element_type="widget" data-widget_type="premium-contact-form.default">
				<div class="elementor-widget-container">
			
			<div class="premium-cf7-container">
				<div role="form" class="wpcf7" id="wpcf7-f1144-o1" lang="en-US" dir="ltr">
<div class="screen-reader-response"><p role="status" aria-live="polite" aria-atomic="true"></p> <ul></ul></div>
<form action="https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/#wpcf7-f1144-o1" method="post" class="wpcf7-form init" novalidate="novalidate" data-status="init">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="1144" />
<input type="hidden" name="_wpcf7_version" value="5.6.3" />
<input type="hidden" name="_wpcf7_locale" value="en_US" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f1144-o1" />
<input type="hidden" name="_wpcf7_container_post" value="0" />
<input type="hidden" name="_wpcf7_posted_data_hash" value="" />
</div>
<div class="field-btn-container"><span class="wpcf7-form-control-wrap" data-name="your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email" /></span> <input type="submit" value="Subscribe" class="wpcf7-form-control has-spinner wpcf7-submit" /></div>
<div class="wpcf7-response-output" aria-hidden="true"></div></form></div>			</div>

					</div>
				</div>
				<div class="elementor-element elementor-element-566788f elementor-shape-square e-grid-align-left elementor-grid-0 elementor-widget elementor-widget-social-icons" data-id="566788f" data-element_type="widget" data-widget_type="social-icons.default">
				<div class="elementor-widget-container">
			<style>/*! elementor - v3.7.3 - 29-08-2022 */
.elementor-widget-social-icons.elementor-grid-0 .elementor-widget-container,.elementor-widget-social-icons.elementor-grid-mobile-0 .elementor-widget-container,.elementor-widget-social-icons.elementor-grid-tablet-0 .elementor-widget-container{line-height:1;font-size:0}.elementor-widget-social-icons:not(.elementor-grid-0):not(.elementor-grid-tablet-0):not(.elementor-grid-mobile-0) .elementor-grid{display:inline-grid}.elementor-widget-social-icons .elementor-grid{grid-column-gap:var(--grid-column-gap,5px);grid-row-gap:var(--grid-row-gap,5px);grid-template-columns:var(--grid-template-columns);-webkit-box-pack:var(--justify-content,center);-ms-flex-pack:var(--justify-content,center);justify-content:var(--justify-content,center);justify-items:var(--justify-content,center)}.elementor-icon.elementor-social-icon{font-size:var(--icon-size,25px);line-height:var(--icon-size,25px);width:calc(var(--icon-size, 25px) + (2 * var(--icon-padding, .5em)));height:calc(var(--icon-size, 25px) + (2 * var(--icon-padding, .5em)))}.elementor-social-icon{--e-social-icon-icon-color:#fff;display:-webkit-inline-box;display:-ms-inline-flexbox;display:inline-flex;background-color:#818a91;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;text-align:center;cursor:pointer}.elementor-social-icon i{color:var(--e-social-icon-icon-color)}.elementor-social-icon svg{fill:var(--e-social-icon-icon-color)}.elementor-social-icon:last-child{margin:0}.elementor-social-icon:hover{opacity:.9;color:#fff}.elementor-social-icon-android{background-color:#a4c639}.elementor-social-icon-apple{background-color:#999}.elementor-social-icon-behance{background-color:#1769ff}.elementor-social-icon-bitbucket{background-color:#205081}.elementor-social-icon-codepen{background-color:#000}.elementor-social-icon-delicious{background-color:#39f}.elementor-social-icon-deviantart{background-color:#05cc47}.elementor-social-icon-digg{background-color:#005be2}.elementor-social-icon-dribbble{background-color:#ea4c89}.elementor-social-icon-elementor{background-color:#d30c5c}.elementor-social-icon-envelope{background-color:#ea4335}.elementor-social-icon-facebook,.elementor-social-icon-facebook-f{background-color:#3b5998}.elementor-social-icon-flickr{background-color:#0063dc}.elementor-social-icon-foursquare{background-color:#2d5be3}.elementor-social-icon-free-code-camp,.elementor-social-icon-freecodecamp{background-color:#006400}.elementor-social-icon-github{background-color:#333}.elementor-social-icon-gitlab{background-color:#e24329}.elementor-social-icon-globe{background-color:#818a91}.elementor-social-icon-google-plus,.elementor-social-icon-google-plus-g{background-color:#dd4b39}.elementor-social-icon-houzz{background-color:#7ac142}.elementor-social-icon-instagram{background-color:#262626}.elementor-social-icon-jsfiddle{background-color:#487aa2}.elementor-social-icon-link{background-color:#818a91}.elementor-social-icon-linkedin,.elementor-social-icon-linkedin-in{background-color:#0077b5}.elementor-social-icon-medium{background-color:#00ab6b}.elementor-social-icon-meetup{background-color:#ec1c40}.elementor-social-icon-mixcloud{background-color:#273a4b}.elementor-social-icon-odnoklassniki{background-color:#f4731c}.elementor-social-icon-pinterest{background-color:#bd081c}.elementor-social-icon-product-hunt{background-color:#da552f}.elementor-social-icon-reddit{background-color:#ff4500}.elementor-social-icon-rss{background-color:#f26522}.elementor-social-icon-shopping-cart{background-color:#4caf50}.elementor-social-icon-skype{background-color:#00aff0}.elementor-social-icon-slideshare{background-color:#0077b5}.elementor-social-icon-snapchat{background-color:#fffc00}.elementor-social-icon-soundcloud{background-color:#f80}.elementor-social-icon-spotify{background-color:#2ebd59}.elementor-social-icon-stack-overflow{background-color:#fe7a15}.elementor-social-icon-steam{background-color:#00adee}.elementor-social-icon-stumbleupon{background-color:#eb4924}.elementor-social-icon-telegram{background-color:#2ca5e0}.elementor-social-icon-thumb-tack{background-color:#1aa1d8}.elementor-social-icon-tripadvisor{background-color:#589442}.elementor-social-icon-tumblr{background-color:#35465c}.elementor-social-icon-twitch{background-color:#6441a5}.elementor-social-icon-twitter{background-color:#1da1f2}.elementor-social-icon-viber{background-color:#665cac}.elementor-social-icon-vimeo{background-color:#1ab7ea}.elementor-social-icon-vk{background-color:#45668e}.elementor-social-icon-weibo{background-color:#dd2430}.elementor-social-icon-weixin{background-color:#31a918}.elementor-social-icon-whatsapp{background-color:#25d366}.elementor-social-icon-wordpress{background-color:#21759b}.elementor-social-icon-xing{background-color:#026466}.elementor-social-icon-yelp{background-color:#af0606}.elementor-social-icon-youtube{background-color:#cd201f}.elementor-social-icon-500px{background-color:#0099e5}.elementor-shape-rounded .elementor-icon.elementor-social-icon{border-radius:10%}.elementor-shape-circle .elementor-icon.elementor-social-icon{border-radius:50%}</style>		<div class="elementor-social-icons-wrapper elementor-grid">
							<span class="elementor-grid-item">
					<a class="elementor-icon elementor-social-icon elementor-social-icon-twitter elementor-repeater-item-a346a69" target="_blank">
						<span class="elementor-screen-only">Twitter</span>
						<i class="fab fa-twitter"></i>					</a>
				</span>
							<span class="elementor-grid-item">
					<a class="elementor-icon elementor-social-icon elementor-social-icon-discord elementor-repeater-item-1849c40" target="_blank">
						<span class="elementor-screen-only">Discord</span>
						<i class="fab fa-discord"></i>					</a>
				</span>
							<span class="elementor-grid-item">
					<a class="elementor-icon elementor-social-icon elementor-social-icon-medium elementor-repeater-item-5958a94" target="_blank">
						<span class="elementor-screen-only">Medium</span>
						<i class="fab fa-medium"></i>					</a>
				</span>
							<span class="elementor-grid-item">
					<a class="elementor-icon elementor-social-icon elementor-social-icon-telegram elementor-repeater-item-d084222" target="_blank">
						<span class="elementor-screen-only">Telegram</span>
						<i class="fab fa-telegram"></i>					</a>
				</span>
					</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-52bc7b8 elementor-section-content-middle elementor-hidden-desktop elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="52bc7b8" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-eca0efb" data-id="eca0efb" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-ef28d9f elementor-widget elementor-widget-image" data-id="ef28d9f" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="287" height="49" src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/2022/09/logo.png?time=1664353118" class="attachment-medium size-medium" alt="" loading="lazy" />															</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-5bf936b" data-id="5bf936b" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-8f37d27 elementor-widget elementor-widget-heading" data-id="8f37d27" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h3 class="elementor-heading-title elementor-size-default">Newsletter</h3>		</div>
				</div>
				<div class="elementor-element elementor-element-e5b0b26 elementor-widget elementor-widget-text-editor" data-id="e5b0b26" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							Sign up for our newsletter to get the latest updates &amp; special offers						</div>
				</div>
				<div class="elementor-element elementor-element-d08a437 elementor-widget elementor-widget-premium-contact-form" data-id="d08a437" data-element_type="widget" data-widget_type="premium-contact-form.default">
				<div class="elementor-widget-container">
			
			<div class="premium-cf7-container">
				<div role="form" class="wpcf7" id="wpcf7-f1144-o2" lang="en-US" dir="ltr">
<div class="screen-reader-response"><p role="status" aria-live="polite" aria-atomic="true"></p> <ul></ul></div>
<form action="https://ihk.8a1.myftpupload.com/metaverse-space/blog-page/#wpcf7-f1144-o2" method="post" class="wpcf7-form init" novalidate="novalidate" data-status="init">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="1144" />
<input type="hidden" name="_wpcf7_version" value="5.6.3" />
<input type="hidden" name="_wpcf7_locale" value="en_US" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f1144-o2" />
<input type="hidden" name="_wpcf7_container_post" value="0" />
<input type="hidden" name="_wpcf7_posted_data_hash" value="" />
</div>
<div class="field-btn-container"><span class="wpcf7-form-control-wrap" data-name="your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email" /></span> <input type="submit" value="Subscribe" class="wpcf7-form-control has-spinner wpcf7-submit" /></div>
<div class="wpcf7-response-output" aria-hidden="true"></div></form></div>			</div>

					</div>
				</div>
				<div class="elementor-element elementor-element-56daa6c elementor-shape-square e-grid-align-left e-grid-align-mobile-center elementor-grid-0 elementor-widget elementor-widget-social-icons" data-id="56daa6c" data-element_type="widget" data-widget_type="social-icons.default">
				<div class="elementor-widget-container">
					<div class="elementor-social-icons-wrapper elementor-grid">
							<span class="elementor-grid-item">
					<a class="elementor-icon elementor-social-icon elementor-social-icon-twitter elementor-repeater-item-a346a69" target="_blank">
						<span class="elementor-screen-only">Twitter</span>
						<i class="fab fa-twitter"></i>					</a>
				</span>
							<span class="elementor-grid-item">
					<a class="elementor-icon elementor-social-icon elementor-social-icon-discord elementor-repeater-item-1849c40" target="_blank">
						<span class="elementor-screen-only">Discord</span>
						<i class="fab fa-discord"></i>					</a>
				</span>
							<span class="elementor-grid-item">
					<a class="elementor-icon elementor-social-icon elementor-social-icon-medium elementor-repeater-item-5958a94" target="_blank">
						<span class="elementor-screen-only">Medium</span>
						<i class="fab fa-medium"></i>					</a>
				</span>
							<span class="elementor-grid-item">
					<a class="elementor-icon elementor-social-icon elementor-social-icon-telegram elementor-repeater-item-d084222" target="_blank">
						<span class="elementor-screen-only">Telegram</span>
						<i class="fab fa-telegram"></i>					</a>
				</span>
					</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-1456151 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="1456151" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-ba10db6" data-id="ba10db6" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-ba68258 elementor-widget elementor-widget-text-editor" data-id="ba68258" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p class="p1">Copyright © 2022 <span style="color:#fff">Metaverse Space</span> All rights reserved</p>						</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-c02da02" data-id="c02da02" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-bd6ef14 elementor-widget elementor-widget-text-editor" data-id="bd6ef14" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<p>Privacy &amp; Cookies          Terms Of Use      About</p>						</div>
				</div>
					</div>
		</div>
							</div>
		</section>
							</div>
		</div>		</footer>
		</div><!-- #page -->
        <div id="wpfront-scroll-top-container">
            <img src="https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/wpfront-scroll-top/images/icons/117.png" alt="" />        </div>
                <script type="text/javascript">
            function wpfront_scroll_top_init() {
                if (typeof wpfront_scroll_top === "function" && typeof jQuery !== "undefined") {
                    wpfront_scroll_top({"scroll_offset":100,"button_width":0,"button_height":0,"button_opacity":0.8000000000000000444089209850062616169452667236328125,"button_fade_duration":200,"scroll_duration":400,"location":1,"marginX":20,"marginY":20,"hide_iframe":false,"auto_hide":false,"auto_hide_after":2,"button_action":"top","button_action_element_selector":"","button_action_container_selector":"html, body","button_action_element_offset":0});
                } else {
                    setTimeout(wpfront_scroll_top_init, 100);
                }
            }
            wpfront_scroll_top_init();
        </script>
        <link rel='stylesheet' id='elementor-post-1254-css'  href='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/elementor/css/post-1254.css?ver=1664352894&amp;time=1664353118' media='all' />
<script id='astra-theme-js-js-extra'>
var astra = {"break_point":"921","isRtl":""};
</script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/themes/astra/assets/js/minified/frontend.min.js?ver=3.9.1&amp;time=1664353118' id='astra-theme-js-js'></script>
<script id='pa-frontend-js-extra'>
var PremiumSettings = {"ajaxurl":"https:\/\/ihk.8a1.myftpupload.com\/wp-admin\/admin-ajax.php","nonce":"619668c9fa"};
</script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/premium-addons-elementor/pa-frontend-471e26a27.min.js?ver=1664421135&amp;time=1664353118' id='pa-frontend-js'></script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/coblocks/dist/js/coblocks-animation.js?ver=2.24.2&amp;time=1664353118' id='coblocks-animation-js'></script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/contact-form-7/includes/swv/js/index.js?ver=5.6.3&amp;time=1664353118' id='swv-js'></script>
<script id='contact-form-7-js-extra'>
var wpcf7 = {"api":{"root":"https:\/\/ihk.8a1.myftpupload.com\/wp-json\/","namespace":"contact-form-7\/v1"}};
</script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/contact-form-7/includes/js/index.js?ver=5.6.3&amp;time=1664353118' id='contact-form-7-js'></script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/wpfront-scroll-top/js/wpfront-scroll-top.min.js?ver=2.0.7.08086&amp;time=1664353118' id='wpfront-scroll-top-js'></script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/wpmss/wpmssab.min.js?ver=1662387883&amp;time=1664353118' id='wpmssab-js'></script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/mousewheel-smooth-scroll/js/SmoothScroll.min.js?ver=1.4.10&amp;time=1664353118' id='SmoothScroll-js'></script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/uploads/wpmss/wpmss.min.js?ver=1662387883&amp;time=1664353118' id='wpmss-js'></script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/header-footer-elementor/inc/js/frontend.js?ver=1.6.13&amp;time=1664353118' id='hfe-frontend-js-js'></script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/elementor/assets/js/webpack.runtime.min.js?ver=3.7.3&amp;time=1664353118' id='elementor-webpack-runtime-js'></script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/elementor/assets/js/frontend-modules.min.js?ver=3.7.3&amp;time=1664353118' id='elementor-frontend-modules-js'></script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2&amp;time=1664353118' id='elementor-waypoints-js'></script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-includes/js/jquery/ui/core.min.js?ver=1.13.1&amp;time=1664353118' id='jquery-ui-core-js'></script>
<script id='elementor-frontend-js-before'>
var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false,"isScriptDebug":false},"i18n":{"shareOnFacebook":"Share on Facebook","shareOnTwitter":"Share on Twitter","pinIt":"Pin it","download":"Download","downloadImage":"Download image","fullscreen":"Fullscreen","zoom":"Zoom","share":"Share","playVideo":"Play Video","previous":"Previous","next":"Next","close":"Close"},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"responsive":{"breakpoints":{"mobile":{"label":"Mobile","value":767,"default_value":767,"direction":"max","is_enabled":true},"mobile_extra":{"label":"Mobile Extra","value":880,"default_value":880,"direction":"max","is_enabled":false},"tablet":{"label":"Tablet","value":1024,"default_value":1024,"direction":"max","is_enabled":true},"tablet_extra":{"label":"Tablet Extra","value":1200,"default_value":1200,"direction":"max","is_enabled":false},"laptop":{"label":"Laptop","value":1366,"default_value":1366,"direction":"max","is_enabled":false},"widescreen":{"label":"Widescreen","value":2400,"default_value":2400,"direction":"min","is_enabled":false}}},"version":"3.7.3","is_static":false,"experimentalFeatures":{"e_dom_optimization":true,"e_optimized_assets_loading":true,"e_optimized_css_loading":true,"a11y_improvements":true,"additional_custom_breakpoints":true,"e_import_export":true,"e_hidden_wordpress_widgets":true,"landing-pages":true,"elements-color-picker":true,"favorite-widgets":true,"admin-top-bar":true},"urls":{"assets":"https:\/\/ihk.8a1.myftpupload.com\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"editorPreferences":[]},"kit":{"active_breakpoints":["viewport_mobile","viewport_tablet"],"global_image_lightbox":"yes","lightbox_enable_counter":"yes","lightbox_enable_fullscreen":"yes","lightbox_enable_zoom":"yes","lightbox_enable_share":"yes","lightbox_title_src":"title","lightbox_description_src":"description"},"post":{"id":1093,"title":"Blog%20Page%20%E2%80%93%20Fellaz","excerpt":"","featuredImage":false}};
</script>
<script src='https://secureservercdn.net/198.71.190.114/ihk.8a1.myftpupload.com/wp-content/plugins/elementor/assets/js/frontend.min.js?ver=3.7.3&amp;time=1664353118' id='elementor-frontend-js'></script>
			<script>
			/(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())},!1);
			</script>
					<script>'undefined'=== typeof _trfq || (window._trfq = []);'undefined'=== typeof _trfd && (window._trfd=[]),_trfd.push({'tccl.baseHost':'secureserver.net'}),_trfd.push({'ap':'wpaas'},{'server':'b8127770-513f-0487-f4e6-1f4ebf92fb61.secureserver.net'},{'pod':'P3NLWPPOD10'},{'storage':'p3cephmah004pod10_data10'},{'xid':'44806276'},{'wp':'6.0.2'},{'php':'8.0.22'},{'loggedin':'0'},{'cdn':'1'},{'builder':'elementor'},{'theme':'astra'},{'wds':'0'},{'wp_alloptions_count':'227'},{'wp_alloptions_bytes':'82637'})</script>
		<script>window.addEventListener('click', function (elem) { var _elem$target, _elem$target$dataset, _window, _window$_trfq; return (elem === null || elem === void 0 ? void 0 : (_elem$target = elem.target) === null || _elem$target === void 0 ? void 0 : (_elem$target$dataset = _elem$target.dataset) === null || _elem$target$dataset === void 0 ? void 0 : _elem$target$dataset.eid) && ((_window = window) === null || _window === void 0 ? void 0 : (_window$_trfq = _window._trfq) === null || _window$_trfq === void 0 ? void 0 : _window$_trfq.push(["cmdLogEvent", "click", elem.target.dataset.eid]));});</script>
		<script src='https://img1.wsimg.com/tcc/tcc_l.combined.1.0.6.min.js'></script>
		<script src='https://img1.wsimg.com/traffic-assets/js/tccl-tti.min.js' onload="window.tti.calculateTTI()"></script>
			</body>

<!-- Mirrored from ihk.8a1.myftpupload.com/metaverse-space/blog-page/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 29 Sep 2022 03:12:16 GMT -->
</html>
